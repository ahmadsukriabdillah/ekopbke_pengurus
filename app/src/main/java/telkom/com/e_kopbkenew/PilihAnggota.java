package telkom.com.e_kopbkenew;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import telkom.com.e_kopbkenew.Adapter.AdapterAnggota;
import telkom.com.e_kopbkenew.Data.Anggota;
import telkom.com.e_kopbkenew.Helper.AppConfig;
import telkom.com.e_kopbkenew.Helper.AppController;
import telkom.com.e_kopbkenew.Helper.Connection;
import telkom.com.e_kopbkenew.Helper.SessionManager;

public class PilihAnggota extends AppCompatActivity implements SearchView.OnQueryTextListener,Connection.ConnectivityReceiverListener {
    private static ArrayList<Anggota> anggota;
    private RecyclerView rv;
    private CoordinatorLayout cl;
    private static AdapterAnggota adapter;
    private ProgressDialog pDialog;
    private SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_anggota);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Pilih Anggota");
        setSupportActionBar(toolbar);
        cl = (CoordinatorLayout) findViewById(R.id.cl);
        rv = (RecyclerView) findViewById(R.id.rv);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setTitle("Loading..");
        anggota = new ArrayList<>();
        session = new SessionManager(this);
        adapter = new AdapterAnggota(getApplicationContext(),anggota);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(mLayoutManager);
        //rv.addItemDecoration(new DividerItem(getApplicationContext(), LinearLayoutManager.VERTICAL));
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setAdapter(adapter);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fetch(session.getNAMA(),session.getUID());
        rv.addOnItemTouchListener(new AdapterAnggota.RecyclerTouchListener(getApplicationContext(), rv, new AdapterAnggota.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent a = new Intent(getApplicationContext(),MasterAnggota.class);
                a.putExtra("data",adapter.gItm(position));
                startActivity(a);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_kamus, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }



    private void fetch(final String uid, final String cif) {

        String endpoint = AppConfig.LIST_ANGGOTA+"?uid="+uid+"&kode=ItemCib&cif="+cif;
        pDialog.setMessage("Loading ...");
        pDialog.show();

        JsonArrayRequest req = new JsonArrayRequest(endpoint,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("erorr", response.toString());
                        pDialog.hide();

                        anggota.clear();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                Anggota a = new Anggota();
                                a.setCIB(object.getString("CIB"));
                                a.setID(object.getString("ID"));
                                a.setALAMAT(object.getString("ALAMAT"));
                                a.setKI(object.getString("KI"));
                                a.setNAMA(object.getString("NAMA"));
                                a.setNAMA_INSTANSI(object.getString("NAMA_INSTANSI"));
                                a.setNIP(object.getString("NIP"));
                                a.setNOID(object.getString("NO_ID"));
                                a.setTGL_EXPIRED(object.getString("TGLEXPIRED"));


                                anggota.add(a);

                            } catch (JSONException e) {
                                //Log.e("ero", "Json parsing error: " + e.getMessage());
                            }


                        }
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("Erorr", "Error: " + error.getMessage());

                pDialog.hide();
                showSnackbar();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);
    }

    private void showSnackbar(){
        final Snackbar snackbar = Snackbar.make(cl,"Terjadi Kesalahan Jaringan",Snackbar.LENGTH_INDEFINITE)
                .setAction("COBA LAGI", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        fetch(session.getNAMA(),session.getUID());
                        //snackbar.dismiss();
                    }
                });
        snackbar.show();
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showNoInternet(isConnected);
    }

    private void showNoInternet(boolean status) {
        final Snackbar snackbar = Snackbar.make(cl,"Tidak Ada Konektifitas",Snackbar.LENGTH_INDEFINITE)
                .setAction("Aktifkan", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setAction(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS);
                        startActivity(intent);
                    }
                });

        if(!status){
            snackbar.show();
        }else{
            snackbar.dismiss();
        }


    }


}
