package telkom.com.e_kopbkenew;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import telkom.com.e_kopbkenew.Adapter.AdapterSaldoPinjaman;
import telkom.com.e_kopbkenew.Adapter.HomeMenuAdapter;
import telkom.com.e_kopbkenew.Data.HomeMenu;
import telkom.com.e_kopbkenew.Data.Pinjaman;
import telkom.com.e_kopbkenew.Data.SpinnerData;
import telkom.com.e_kopbkenew.Helper.AppConfig;
import telkom.com.e_kopbkenew.Helper.AppController;
import telkom.com.e_kopbkenew.Helper.Connection;
import telkom.com.e_kopbkenew.Helper.SessionManager;

public class Modal extends AppCompatActivity implements Connection.ConnectivityReceiverListener {
    private float mx, my;
    private float curX, curY;
    private static ArrayList<SpinnerData> dataJenis;
    private Spinner spinnerJenis;
    private ArrayAdapter<SpinnerData> adapterJenis;
    private SessionManager session;
    private ProgressDialog pDialog;
    private RecyclerView rv;
    private RecyclerView rv2;
    private HomeMenuAdapter adapter;
    private ArrayList<HomeMenu> albumList;
    private ArrayList<Pinjaman> pinjaman;
    private AdapterSaldoPinjaman adapterPinjaman;
    private ScrollView vScroll;
    private HorizontalScrollView hScroll;



    //gestur
    //private float mScale = 1f;
    //private ScaleGestureDetector mScaleDetector;
    //private float scale = 0.5f;
    private float scale = 1f;
    private ScaleGestureDetector detector;
    private Toolbar toolbar;
    private CoordinatorLayout cl;
    private static String temp;
    private static int pil;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modal);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        session = new SessionManager(this);
        pDialog = new ProgressDialog(this);
        rv2 = (RecyclerView) findViewById(R.id.rv2);
        albumList = new ArrayList<>();
        adapter = new HomeMenuAdapter(getApplicationContext(),albumList);
        pinjaman = new ArrayList<>();

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 4);
        rv2.setLayoutManager(mLayoutManager);
        rv2.setItemAnimator(new DefaultItemAnimator());
        rv2.setAdapter(adapter);
        cl = (CoordinatorLayout) findViewById(R.id.cl);
        adapterPinjaman = new AdapterSaldoPinjaman(getApplicationContext(),pinjaman);
        rv = (RecyclerView) findViewById(R.id.rv);
        vScroll = (ScrollView)findViewById(R.id.vScroll);
        hScroll = (HorizontalScrollView) findViewById(R.id.hScroll);
        //rv.setScaleX(0.3f);
        //rv.setScaleX(0.3f);
        //gestureDetector = new GestureDetector(this, new GestureListener());
        detector = new ScaleGestureDetector(this,new ScaleListener());
        switch (getIntent().getIntExtra("data",0)){
            case 0:
                toolbar.setTitle("Master Simpanan");
                setSupportActionBar(toolbar);
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rv.setAdapter(adapterPinjaman);

        rv2.addOnItemTouchListener(new HomeMenuAdapter.RecyclerTouchListener(getApplicationContext(), rv, new HomeMenuAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                switch (position){

                    case 0:
                        fetchJenis(session.getNAMA(),session.getUID(),0);
                        pil = 0;
                        break;

                    case 1:

                        fetchJenis(session.getNAMA(),session.getUID(),0);
                        break;

                    case 2:
                        fetchJenis(session.getNAMA(),session.getUID(),0);
                        break;


                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        dataJenis = new ArrayList<>();
        spinnerJenis = (Spinner) findViewById(R.id.spinner2);
        adapterJenis = new ArrayAdapter<>(this,R.layout.spinner,dataJenis);
        spinnerJenis.setPrompt("-- Pilih Jenis --");
        spinnerJenis.setAdapter(adapterJenis);
        spinnerJenis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0){

                }else{
                    switch (getIntent().getIntExtra("data",0)){
                        case 0:
                            fetchLaporanSimpanan(session.getNAMA(),session.getUID(),dataJenis.get(i).getId());
                            temp = dataJenis.get(i).getId();

                            break;
                        case 1:
                            fetchLaporanDeposito(session.getNAMA(),session.getUID(),dataJenis.get(i).getId());
                            temp = dataJenis.get(i).getId();
                            break;
                        case 2:
                            fetchLaporanPiutang(session.getNAMA(),session.getUID(),dataJenis.get(i).getId());
                            temp = dataJenis.get(i).getId();

                            break;
                        case 3:
                            fetchLaporanHutang(session.getNAMA(),session.getUID(),dataJenis.get(i).getId());
                            temp = dataJenis.get(i).getId();

                            break;
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



    }

    private void initData() {
        albumList.add(new HomeMenu(R.drawable.student,"Master Simpanan","1"));
        albumList.add(new HomeMenu(R.drawable.documentation,"Master Deposito","2"));
        albumList.add(new HomeMenu(R.drawable.coins,"Master Piutang","3"));
        albumList.add(new HomeMenu(R.drawable.diagram,"Master Hutang","4"));
        adapter.notifyDataSetChanged();

    }

    private void fetchLaporanHutang(String nama, String uid, String id) {
    }

    private void fetchLaporanPiutang(String nama, String uid, String id) {
    }

    private void fetchLaporanDeposito(String nama, String uid, String id) {
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        super.dispatchTouchEvent(ev);
        //mScaleDetector.onTouchEvent(ev);
        //gestureDetector.onTouchEvent(ev);
        detector.onTouchEvent(ev);
        switch (ev.getAction()) {

            case MotionEvent.ACTION_DOWN:
                mx = ev.getX();
                my = ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                curX = ev.getX();
                curY = ev.getY();
                vScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                hScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                mx = curX;
                my = curY;
                break;
            case MotionEvent.ACTION_UP:
                curX = ev.getX();
                curY = ev.getY();
                vScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                hScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                break;
        }

        return true;
    }

    private void showSnackbar(){
        final Snackbar snackbar = Snackbar.make(cl,"Terjadi Kesalahan Jaringan",Snackbar.LENGTH_INDEFINITE)
                .setAction("COBA LAGI", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (getIntent().getIntExtra("data",0)){
                            case 0:
                                fetchLaporanSimpanan(session.getNAMA(),session.getUID(),temp);


                                break;
                            case 1:
                                fetchLaporanDeposito(session.getNAMA(),session.getUID(),temp);

                                break;
                            case 2:
                                fetchLaporanPiutang(session.getNAMA(),session.getUID(),temp);


                                break;
                            case 3:
                                fetchLaporanHutang(session.getNAMA(),session.getUID(),temp);


                                break;
                        }
                    }
                });
        snackbar.show();
    }

    private void fetchLaporanSimpanan(final String uid,final String cif,final String jenis) {
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.M_SIM_JENIS+"?cif="+cif+"&uid="+uid+"&kode=MasterSimpanan&jenis="+jenis, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    pinjaman.clear();
                    ArrayList<String> sheader = new ArrayList<>();
                    sheader.add("NOSIMP");
                    sheader.add("CIB");
                    sheader.add("NAMA");
                    sheader.add("DEBET");
                    sheader.add("KREDIT");
                    sheader.add("SALDO");
                    pinjaman.add(new Pinjaman(sheader,Pinjaman.TYPE_SUBHEADER));

                    JSONArray jObj = new JSONArray(response);
                    for(int i = 0;i<jObj.length();i++) {
                        JSONObject object = jObj.getJSONObject(i);
                        pinjaman.add(new Pinjaman(i, object.getString("NOSIMP"), object.getString("CIB"), object.getString("NAMA"), object.getString("DEBET"), object.getString("KREDIT"), object.getString("SALDO")));
                    }
                    adapterPinjaman.notifyDataSetChanged();



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
                showSnackbar();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void fetchJenis(final String uid,final String cif,int post) {
        String endpoint = "";
        switch (post){
            case 0:
                endpoint = AppConfig.M_SIM_JENIS+"?uid="+uid+"&kode=JenisSimpanan&cif="+cif;
                break;
            case 1:
                endpoint = AppConfig.M_DEP_JENIS+"?uid="+uid+"&kode=ItemJenis&cif="+cif;
                break;
            case 2:
                endpoint = AppConfig.M_HUT_JENIS+"?uid="+uid+"&kode=ItemJenis&cif="+cif;
                break;
            case 3:
                endpoint = AppConfig.M_PIU_JENIS+"?uid="+uid+"&kode=JenisSimpanan&cif="+cif;
                break;
        }

        JsonArrayRequest req = new JsonArrayRequest(endpoint,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        adapterJenis.clear();
                        adapterJenis.add(new SpinnerData("10020202","-- Pilih Jenis --"));
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                SpinnerData a = new SpinnerData(object.getString("ACC"),object.getString("ACC")+" "+object.getString("KETERANGAN"));
                                adapterJenis.add(a);

                            } catch (JSONException e) {
                                Log.e("ero", "Json parsing error: " + e.getMessage());
                            }
                        }
                        adapterJenis.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Erorr", "Error: " + error.getMessage());
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);

    }


    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {


        float onScaleBegin = 0;
        float onScaleEnd = 0;

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scale *= detector.getScaleFactor();
            rv.setScaleX(scale);
            rv.setScaleY(scale);
            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {

            //Toast.makeText(getApplicationContext(),"Scale Begin" ,Toast.LENGTH_SHORT).show();
            onScaleBegin = scale;

            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {

            //Toast.makeText(getApplicationContext(),"Scale Ended",Toast.LENGTH_SHORT).show();
            onScaleEnd = scale;

            if (onScaleEnd > onScaleBegin){
                //  Toast.makeText(getApplicationContext(),"Scaled Up by a factor of  " + String.valueOf( onScaleEnd / onScaleBegin ), Toast.LENGTH_SHORT  ).show();
            }

            if (onScaleEnd < onScaleBegin){
                //Toast.makeText(getApplicationContext(),"Scaled Down by a factor of  " + String.valueOf( onScaleBegin / onScaleEnd ), Toast.LENGTH_SHORT  ).show();
            }

            super.onScaleEnd(detector);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showNoInternet(isConnected);
    }

    private void showNoInternet(boolean status) {
        final Snackbar snackbar = Snackbar.make(cl,"Tidak Ada Konektifitas",Snackbar.LENGTH_INDEFINITE)
                .setAction("Aktifkan", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setAction(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS);
                        startActivity(intent);
                    }
                });

        if(!status){
            snackbar.show();
        }else{
            snackbar.dismiss();
        }


    }

}
