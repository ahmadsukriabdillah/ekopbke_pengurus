package telkom.com.e_kopbkenew;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import telkom.com.e_kopbkenew.Adapter.AdapterAnggotaTransaksi;
import telkom.com.e_kopbkenew.Data.Anggota;
import telkom.com.e_kopbkenew.Data.Neraca;
import telkom.com.e_kopbkenew.Helper.AppConfig;
import telkom.com.e_kopbkenew.Helper.AppController;
import telkom.com.e_kopbkenew.Helper.Connection;
import telkom.com.e_kopbkenew.Helper.SessionManager;

public class MasterAnggota extends AppCompatActivity  implements Connection.ConnectivityReceiverListener{
    private float mx, my;
    private float curX, curY;
    private Anggota anggota;
    private TextView noangota,nama,namainstansi,no,noid,alamat;
    private ScrollView vScroll;
    private HorizontalScrollView hScroll;
    private RecyclerView rv;
    private SessionManager session;
    private ArrayList<Neraca> neraca;
    private AdapterAnggotaTransaksi adapter;



    //gestur
    //private float mScale = 1f;
    //private ScaleGestureDetector mScaleDetector;
    //private float scale = 0.5f;
    private float scale = 1f;
    private ScaleGestureDetector detector;
    GestureDetector gestureDetector;
    private CoordinatorLayout cl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_master_anggota);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Detail Anggota");
        setSupportActionBar(toolbar);
        cl = (CoordinatorLayout) findViewById(R.id.cl);
        session = new SessionManager(this);
        neraca = new ArrayList<>();
        adapter = new AdapterAnggotaTransaksi(getApplicationContext(),neraca);
        anggota = getIntent().getParcelableExtra("data");
        noangota = (TextView) findViewById(R.id.textView18);
        nama = (TextView) findViewById(R.id.nama);
        namainstansi = (TextView) findViewById(R.id.instansi);
        no = (TextView) findViewById(R.id.textView26);
        noid = (TextView) findViewById(R.id.noid);
        alamat = (TextView) findViewById(R.id.alamat);
        rv = (RecyclerView) findViewById(R.id.rv);

        vScroll = (ScrollView)findViewById(R.id.vScroll);
        hScroll = (HorizontalScrollView) findViewById(R.id.hScroll);
        //rv.setScaleX(0.3f);
        //rv.setScaleX(0.3f);
        //gestureDetector = new GestureDetector(this, new GestureListener());
        detector = new ScaleGestureDetector(this,new ScaleListener());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(mLayoutManager);
        rv.setAdapter(adapter);


        noangota.setText(anggota.getCIB());
        nama.setText(anggota.getNAMA());
        namainstansi.setText(anggota.getNAMA_INSTANSI());
        no.setText(anggota.getID());
        noid.setText(anggota.getNOID());
        alamat.setText(anggota.getALAMAT());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fetchData(session.getNAMA(),session.getUID(),anggota.getCIB());
    }

    private void fetchData(final String uid,final String cif,final String cib) {
        String tag_string_req = "req_login";



        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.LIST_ANGGOTA+"?uid="+uid+"&cif="+cif+"&kode=Preview&cib="+cib, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());


                try {

                    ArrayList<String> sheader = new ArrayList<>();
                    sheader.add("MASTER");
                    sheader.add("ACC");
                    sheader.add("KETERANGAN");
                    sheader.add("SALDO");

                    neraca.add(new Neraca(sheader,Neraca.TYPE_SUBHEADER));

                    JSONArray jObj = new JSONArray(response);
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);
                        Neraca a = new Neraca();
                        a.setAcc1(object.getString("MASTER"));
                        a.setDeskripsi1(object.getString("ACC"));
                        a.setSaldo1(object.getString("KETERANGAN"));
                        a.setAcc2(object.getString("SALDO"));
                        neraca.add(a);
                    }

                    adapter.notifyDataSetChanged();



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_LONG).show();
                showSnackbar();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


    }

    private void showSnackbar(){
        final Snackbar snackbar = Snackbar.make(cl,"Terjadi Kesalahan Jaringan",Snackbar.LENGTH_INDEFINITE)
                .setAction("COBA LAGI", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        fetchData(session.getNAMA(),session.getUID(),anggota.getCIB());
                        //snackbar.dismiss();
                    }
                });
        snackbar.show();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        super.dispatchTouchEvent(ev);
        //mScaleDetector.onTouchEvent(ev);
        //gestureDetector.onTouchEvent(ev);
        detector.onTouchEvent(ev);
        switch (ev.getAction()) {

            case MotionEvent.ACTION_DOWN:
                mx = ev.getX();
                my = ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                curX = ev.getX();
                curY = ev.getY();
                vScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                hScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                mx = curX;
                my = curY;
                break;
            case MotionEvent.ACTION_UP:
                curX = ev.getX();
                curY = ev.getY();
                vScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                hScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                break;
        }

        return true;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {


        float onScaleBegin = 0;
        float onScaleEnd = 0;

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scale *= detector.getScaleFactor();
            rv.setScaleX(scale);
            rv.setScaleY(scale);
            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {

            //Toast.makeText(getApplicationContext(),"Scale Begin" ,Toast.LENGTH_SHORT).show();
            onScaleBegin = scale;

            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {

            //Toast.makeText(getApplicationContext(),"Scale Ended",Toast.LENGTH_SHORT).show();
            onScaleEnd = scale;

            if (onScaleEnd > onScaleBegin){
                //  Toast.makeText(getApplicationContext(),"Scaled Up by a factor of  " + String.valueOf( onScaleEnd / onScaleBegin ), Toast.LENGTH_SHORT  ).show();
            }

            if (onScaleEnd < onScaleBegin){
                //Toast.makeText(getApplicationContext(),"Scaled Down by a factor of  " + String.valueOf( onScaleBegin / onScaleEnd ), Toast.LENGTH_SHORT  ).show();
            }

            super.onScaleEnd(detector);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showNoInternet(isConnected);
    }

    private void showNoInternet(boolean status) {
        final Snackbar snackbar = Snackbar.make(cl,"Tidak Ada Konektifitas",Snackbar.LENGTH_INDEFINITE)
                .setAction("Aktifkan", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setAction(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS);
                        startActivity(intent);
                    }
                });

        if(!status){
            snackbar.show();
        }else{
            snackbar.dismiss();
        }


    }

}
