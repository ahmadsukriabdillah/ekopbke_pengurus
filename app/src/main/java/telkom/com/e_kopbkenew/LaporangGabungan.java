package telkom.com.e_kopbkenew;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import telkom.com.e_kopbkenew.Adapter.AdapterCashFlow;
import telkom.com.e_kopbkenew.Adapter.AdapterKredibilitas;
import telkom.com.e_kopbkenew.Adapter.AdapterNeraca;
import telkom.com.e_kopbkenew.Adapter.AdapterNeracaLajur;
import telkom.com.e_kopbkenew.Adapter.AdapterPerubahanModal;
import telkom.com.e_kopbkenew.Adapter.AllAdapter;
import telkom.com.e_kopbkenew.Data.AllData;
import telkom.com.e_kopbkenew.Data.CFlow;
import telkom.com.e_kopbkenew.Data.Neraca;
import telkom.com.e_kopbkenew.Data.NeracaLajur;
import telkom.com.e_kopbkenew.Data.PModal;
import telkom.com.e_kopbkenew.Data.SpinnerData;
import telkom.com.e_kopbkenew.Helper.AppConfig;
import telkom.com.e_kopbkenew.Helper.AppController;
import telkom.com.e_kopbkenew.Helper.Connection;
import telkom.com.e_kopbkenew.Helper.SessionManager;

public class LaporangGabungan extends AppCompatActivity implements Connection.ConnectivityReceiverListener {

    private float mx, my;
    private float curX, curY;
    private CoordinatorLayout cl;
    private RecyclerView rv;
    private ScrollView vScroll;
    private HorizontalScrollView hScroll;
    private ProgressDialog pDialog;
    private Spinner spinerJenis,spinerKodeUnit;
    private SessionManager session;
    private LinearLayout lyku;
    private LinearLayout lyjenis;
    private ArrayList<SpinnerData> dataJenis;
    private ArrayList<SpinnerData> dataKu;
    private Toolbar toolbar;
    private float scale = 1f;
    private ScaleGestureDetector detector;
    private TextView txtJenis,txtKodeUnit;

    /**
     * laporang gabungan neraca lajur
     */
    private ArrayList<NeracaLajur> neracaLajur;
    private AdapterNeracaLajur adapterNeracaLajur;

    private ArrayList<Neraca> neraca;
    private AdapterNeraca adapter;


    private ArrayList<CFlow> cFlow;
    private AdapterCashFlow adapterCashFlow;

    private ArrayList<PModal> pModal;
    private AdapterPerubahanModal adapterPerubahanModal;

    private ArrayList<AllData> produktifitas;
    private AllAdapter produktifitasAdapter;

    private AdapterKredibilitas adapterKredibilitas;

    private ArrayAdapter<SpinnerData> jenis;
    private ArrayAdapter<SpinnerData> ku;
    private static int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporang_gabungan);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        cl = (CoordinatorLayout) findViewById(R.id.cl);
        rv = (RecyclerView) findViewById(R.id.rv);
        txtJenis = (TextView) findViewById(R.id.txtJenis);
        txtKodeUnit = (TextView) findViewById(R.id.txtKu);
        dataJenis = new ArrayList<>();
        dataKu = new ArrayList<>();
        spinerJenis = (Spinner) findViewById(R.id.spinner2);
        spinerKodeUnit = (Spinner) findViewById(R.id.spinner3);
        lyku = (LinearLayout) findViewById(R.id.kodeunit);
        lyjenis = (LinearLayout) findViewById(R.id.jenis);
        vScroll = (ScrollView) findViewById(R.id.vScroll);
        hScroll = (HorizontalScrollView) findViewById(R.id.hScroll);
        neraca =  new ArrayList<>();
        adapter =  new AdapterNeraca(getApplicationContext(),neraca);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        session = new SessionManager(this);
        detector = new ScaleGestureDetector(this,new ScaleListener());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setAutoMeasureEnabled(true);
        rv.setLayoutManager(mLayoutManager);

        /**
         * spiner
         *
         */


        jenis = new ArrayAdapter<>(this,R.layout.spinner,dataJenis);
        jenis.setDropDownViewResource(R.layout.spinner); // The drop down view
        spinerJenis.setAdapter(jenis);
        spinerJenis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0){

                }else{

                    if(pos == 0){

                        fetchKU(dataJenis.get(i).getId(),session.getUID(),session.getNAMA());
                        lyku.setVisibility(View.VISIBLE);

                    }else{

                        fetchKU(dataJenis.get(i).getId(),session.getUID(),session.getNAMA());
                        lyku.setVisibility(View.VISIBLE);

                    }
}

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ku = new ArrayAdapter<>(this,R.layout.spinner,dataKu);
        ku.setDropDownViewResource(R.layout.spinner); // The drop down view
        spinerKodeUnit.setAdapter(ku);

        spinerKodeUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0){

                }else {
                    if(pos == 1){
                        laporanKredibilitas();
                    }else{

                        Toast.makeText(getApplicationContext(),"Still Working in it be Patien",Toast.LENGTH_LONG).show();

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        /**
         * neraca lajur
         */



        pModal = new ArrayList<>();
        adapterPerubahanModal = new AdapterPerubahanModal(getApplicationContext(),pModal);

        cFlow = new ArrayList<>();
        adapterCashFlow = new AdapterCashFlow(getApplicationContext(),cFlow);
        produktifitas = new ArrayList<>();
        produktifitasAdapter = new AllAdapter(getApplicationContext(),produktifitas);

        neracaLajur = new ArrayList<>();
        adapterNeracaLajur = new AdapterNeracaLajur(getApplicationContext(),neracaLajur);
        adapterKredibilitas = new AdapterKredibilitas(getApplicationContext(),produktifitas);

        switch (getIntent().getIntExtra("laporan",99)){
            case 0:
                lyjenis.setVisibility(View.VISIBLE);
                toolbar.setTitle("Kolektabilitas");
                setSupportActionBar(toolbar);
                fetchJenis();
                pos = 1;
                rv.setAdapter(adapterKredibilitas);
                break;
            case 2:
                toolbar.setTitle("Cash Flow Gabungan");
                setSupportActionBar(toolbar);
                rv.setAdapter(adapterCashFlow);
                laporanCashFlow(session.getNAMA(),session.getUID());
                break;
            case 3:
                toolbar.setTitle("Laporan Perubahan Modal Gabungan");
                setSupportActionBar(toolbar);
                rv.setAdapter(adapterPerubahanModal);
                laporanPerubahanModal(session.getNAMA(),session.getUID());
                break;

            case 1:
                toolbar.setTitle("Neraca Lajur Gabungan");
                setSupportActionBar(toolbar);
                rv.setAdapter(adapterNeracaLajur);
                laporanNeracaLajur(session.getNAMA(),session.getUID());
                break;

            case 4:
                toolbar.setTitle("Laba / Rugi Gabungan");
                setSupportActionBar(toolbar);
                rv.setAdapter(adapter);
                laporanLabaRugi(session.getNAMA(),session.getUID());
                break;

            case 5:
                toolbar.setTitle("Laporan Neraca Gabungan");
                setSupportActionBar(toolbar);
                rv.setAdapter(adapter);
                laporanNeraca(session.getNAMA(),session.getUID());
                break;
            case 6:
                toolbar.setTitle("Laporan Produktifitas");
                setSupportActionBar(toolbar);
                rv.setAdapter(produktifitasAdapter);
                laporanProduktifitas(session.getNAMA(),session.getUID());
                break;
            case 7:
                toolbar.setTitle("Laporan Kesehatan");
                setSupportActionBar(toolbar);
                fetchJenis();
                pos = 0;
                lyjenis.setVisibility(View.VISIBLE);
                break;
            default:
                finish();
        }
    }

    private void laporanKredibilitas() {
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();
        String add = "?kode=Preview&cif="+session.getUID()+"&jenis="+dataJenis.get(spinerKodeUnit.getSelectedItemPosition()).getId()+"&urutcoll="+dataKu.get(spinerKodeUnit.getSelectedItemPosition()).getId();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.KOLEKTABLITAS + add, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    produktifitas.clear();

                    ArrayList<String> a = new ArrayList<>();
                    a.add("NOPINJ");
                    a.add("NAMA");
                    a.add("ACC");
                    a.add("KETERANGAN");
                    a.add("COLL");
                    a.add("PLAFOND");
                    a.add("OUTSTANDING");
                    produktifitas.add(new AllData(a,AllData.TYPE_SUBHEADER));

                    JSONObject jo = new JSONObject(response);
                    JSONArray jObj = jo.getJSONArray("table");
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);

                        a = new ArrayList<>();
                        a.add(object.getString("NOPINJ"));
                        a.add(object.getString("NAMA"));
                        a.add(object.getString("ACC"));
                        a.add(object.getString("KETERANGAN"));
                        a.add(object.getString("COLL"));
                        a.add(object.getString("PLAFOND"));
                        a.add(object.getString("OUTSTANDING"));
                        produktifitas.add(new AllData(a,AllData.TYPE_CONTENT));
                    }
                    adapterKredibilitas.notifyDataSetChanged();



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
                showSnackbar();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void laporanProduktifitas(final String uid, String cif) {
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.LP +"?kode=Preview&uid="+uid+"&cif="+cif, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    produktifitas.clear();

                    ArrayList<String> a = new ArrayList<>();
                    a.add("ACC");
                    a.add("KETERANGAN");
                    a.add("THNSBL");
                    a.add("JANUARI");
                    a.add("FEBRUARI");
                    a.add("MARET");
                    a.add("APRIL");
                    a.add("MEI");
                    a.add("JUNI");
                    a.add("JULI");
                    a.add("AGUSTUS");
                    a.add("SEPTERMBER");
                    a.add("OKTOBER");
                    a.add("NOVEMBER");
                    a.add("DESEMBER");
                    produktifitas.add(new AllData(a,AllData.TYPE_SUBHEADER));

                    JSONObject jo = new JSONObject(response);
                    JSONArray jObj = jo.getJSONArray("table");
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);

                        a = new ArrayList<>();
                        a.add(object.getString("ACC"));
                        a.add(object.getString("KETERANGAN"));
                        a.add(object.getString("THNSBL"));
                        a.add(object.getString("JANUARI"));
                        a.add(object.getString("FEBRUARI"));
                        a.add(object.getString("MARET"));
                        a.add(object.getString("APRIL"));
                        a.add(object.getString("MEI"));
                        a.add(object.getString("JUNI"));
                        a.add(object.getString("JULI"));
                        a.add(object.getString("AGUSTUS"));
                        a.add(object.getString("SEPTEMBER"));
                        a.add(object.getString("OKTOBER"));
                        a.add(object.getString("NOVEMBER"));
                        a.add(object.getString("DESEMBER"));
                        produktifitas.add(new AllData(a,AllData.TYPE_CONTENT));
                    }
                    produktifitasAdapter.notifyDataSetChanged();



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
                showSnackbar();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


    }


    private void showSnackbar(){
        final Snackbar snackbar = Snackbar.make(cl,"Terjadi Kesalahan Jaringan",Snackbar.LENGTH_INDEFINITE)
                .setAction("COBA LAGI", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (getIntent().getIntExtra("laporan",99)){
                            case 0:
                                lyjenis.setVisibility(View.VISIBLE);
                                toolbar.setTitle("Kolektabilitas");
                                setSupportActionBar(toolbar);
                                fetchJenis();
                                pos = 1;
                                laporanKredibilitas();
                                break;
                            case 2:
                                toolbar.setTitle("Cash Flow Gabungan");
                                setSupportActionBar(toolbar);
                                rv.setAdapter(adapterCashFlow);
                                laporanCashFlow(session.getNAMA(),session.getUID());
                                break;
                            case 3:
                                toolbar.setTitle("Laporan Perubahan Modal Gabungan");
                                setSupportActionBar(toolbar);
                                rv.setAdapter(adapterPerubahanModal);
                                laporanPerubahanModal(session.getNAMA(),session.getUID());
                                break;

                            case 1:
                                toolbar.setTitle("Neraca Lajur Gabungan");
                                setSupportActionBar(toolbar);
                                rv.setAdapter(adapterNeracaLajur);
                                laporanNeracaLajur(session.getNAMA(),session.getUID());
                                break;

                            case 4:
                                toolbar.setTitle("Laba / Rugi Gabungan");
                                setSupportActionBar(toolbar);
                                rv.setAdapter(adapter);
                                laporanLabaRugi(session.getNAMA(),session.getUID());
                                break;

                            case 5:
                                toolbar.setTitle("Laporan Neraca Gabungan");
                                setSupportActionBar(toolbar);
                                rv.setAdapter(adapter);
                                laporanNeraca(session.getNAMA(),session.getUID());
                                break;
                            case 6:
                                toolbar.setTitle("Laporan Produktifitas");
                                setSupportActionBar(toolbar);
                                rv.setAdapter(produktifitasAdapter);
                                laporanProduktifitas(session.getNAMA(),session.getUID());
                                break;
                            case 7:
                                toolbar.setTitle("Laporan Kesehatan");
                                setSupportActionBar(toolbar);
                                fetchJenis();
                                pos = 0;
                                lyjenis.setVisibility(View.VISIBLE);
                                break;
                            default:
                                finish();
                        }
                        //snackbar.dismiss();
                    }
                });
        snackbar.show();
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        super.dispatchTouchEvent(ev);
        //mScaleDetector.onTouchEvent(ev);
        //gestureDetector.onTouchEvent(ev);
        detector.onTouchEvent(ev);
        switch (ev.getAction()) {

            case MotionEvent.ACTION_DOWN:
                mx = ev.getX();
                my = ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                curX = ev.getX();
                curY = ev.getY();
                vScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                hScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                mx = curX;
                my = curY;
                break;
            case MotionEvent.ACTION_UP:
                curX = ev.getX();
                curY = ev.getY();
                vScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                hScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                break;
        }

        return true;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {


        float onScaleBegin = 0;
        float onScaleEnd = 0;

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scale *= detector.getScaleFactor();
            rv.setScaleX(scale);
            rv.setScaleY(scale);
            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {

            //Toast.makeText(getApplicationContext(),"Scale Begin" ,Toast.LENGTH_SHORT).show();
            onScaleBegin = scale;

            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {

            //Toast.makeText(getApplicationContext(),"Scale Ended",Toast.LENGTH_SHORT).show();
            onScaleEnd = scale;

            if (onScaleEnd > onScaleBegin){
                //  Toast.makeText(getApplicationContext(),"Scaled Up by a factor of  " + String.valueOf( onScaleEnd / onScaleBegin ), Toast.LENGTH_SHORT  ).show();
            }

            if (onScaleEnd < onScaleBegin){
                //Toast.makeText(getApplicationContext(),"Scaled Down by a factor of  " + String.valueOf( onScaleBegin / onScaleEnd ), Toast.LENGTH_SHORT  ).show();
            }

            super.onScaleEnd(detector);
        }
    }


    private void laporanNeracaLajur(final String uid,final String cif) {
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.NLG +"?uid="+uid+"&cif="+cif+"&kode=Preview", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    neracaLajur.clear();
                    ArrayList<String> sheader3 = new ArrayList<>();
                    sheader3.add("ASET");
                    sheader3.add("KEWAJIBAN DAN EKUITAS");

                    neracaLajur.add(new NeracaLajur(sheader3, Neraca.TYPE_HEADER));


                    //neraca.add(new Neraca(2,new String[]{"Sub 1","Sub 2","Sub 3","Sub 4","Sub 5","Sub 6"}));
                    JSONObject jo = new JSONObject(response);
                    //JSONArray jObj = new JSONArray(response);
                    JSONArray jObj = jo.getJSONArray("table");
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);
                        neracaLajur.add(new NeracaLajur(i,object.getString("ACC"),object.getString("KETERANGAN"),object.getString("DEBET"),object.getString("KREDIT"),object.getString("DEBET1"),object.getString("KREDIT1"),object.getString("DEBET2"),object.getString("KREDIT2"),object.getString("LDEBET"),object.getString("LKREDIT"),object.getString("NDEBET"),object.getString("NKREDIT")));
                    }
                    /*
                    JSONObject jTot = jo.getJSONObject("total");
                    ArrayList<String> total = new ArrayList<>();

                    total.add(jTot.getString("TDEBET"));
                    total.add(jTot.getString("TKREDIT"));
                    total.add(jTot.getString("TDEBET1"));
                    total.add(jTot.getString("TKREDIT1"));
                    total.add(jTot.getString("TDEBET2"));
                    total.add(jTot.getString("TKREDIT2"));
                    total.add(jTot.getString("TLDEBET"));
                    total.add(jTot.getString("TLKREDIT"));
                    total.add(jTot.getString("NDEBET"));
                    total.add(jTot.getString("TNKREDIT"));
                    neracaLajur.add(new NeracaLajur(total,Neraca.TYPE_TOTAL));
                    */
                    //lyku.setVisibility(View.GONE);
                    //lyjenis.setVisibility(View.GONE);
                    adapterNeracaLajur.notifyDataSetChanged();



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
                showSnackbar();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void laporanLabaRugi(final String uid,final String cif) {
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.LAPORAN_LABA_RUGI+"?uid="+uid+"&cif="+cif+"&kode=Proses", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    neraca.clear();
                    ArrayList<String> sheader3 = new ArrayList<>();
                    sheader3.add("Beban");
                    sheader3.add("Pendapatan");

                    neraca.add(new Neraca(sheader3,Neraca.TYPE_HEADER));
                    ArrayList<String> sheader = new ArrayList<>();
                    sheader.add("ACC");
                    sheader.add("DESKRIPSI");
                    sheader.add("SALDO");
                    sheader.add("ACC");
                    sheader.add("DESKRIPSI");
                    sheader.add("SALDO");
                    neraca.add(new Neraca(sheader,Neraca.TYPE_SUBHEADER));

                    //neraca.add(new Neraca(2,new String[]{"Sub 1","Sub 2","Sub 3","Sub 4","Sub 5","Sub 6"}));
                    JSONObject jo = new JSONObject(response);
                    //JSONArray jObj = new JSONArray(response);
                    JSONArray jObj = jo.getJSONArray("table");
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);
                        neraca.add(new Neraca(i,object.getString("NO1"),object.getString("URAIAN1"),object.getString("ANOMINAL1"),object.getString("NO2"),object.getString("URAIAN2"),object.getString("ANOMINAL2")));
                    }
                    ArrayList<String> total = new ArrayList<>();
                    if(jo.getJSONObject("total").getString("TOTAL").equals("null")){

                    }else{
                        JSONObject jTot = jo.getJSONObject("total");
                        total.add("Total Debit");
                        total.add(jTot.getString("TOTALDEBET"));
                        total.add("Total Kredit");
                        total.add(jTot.getString("TOTALKREDIT"));
                        neraca.add(new Neraca(total,Neraca.TYPE_TOTAL));
                    }
                    //lyku.setVisibility(View.GONE);
                    //lyjenis.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),
                //       error.getMessage(), Toast.LENGTH_LONG).show();
                showSnackbar();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


    }


    private void laporanNeraca(final String uid,final String cif) {
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.LNG+"?uid="+uid+"&cif="+cif+"&kode=Proses", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    neraca.clear();
                    ArrayList<String> sheader3 = new ArrayList<>();
                    sheader3.add("ASET");
                    sheader3.add("KEWAJIBAN DAN EKUITAS");

                    neraca.add(new Neraca(sheader3,Neraca.TYPE_HEADER));
                    ArrayList<String> sheader = new ArrayList<>();
                    sheader.add("ACC");
                    sheader.add("DESKRIPSI");
                    sheader.add("SALDO");
                    sheader.add("ACC");
                    sheader.add("DESKRIPSI");
                    sheader.add("SALDO");
                    neraca.add(new Neraca(sheader,Neraca.TYPE_SUBHEADER));

                    //neraca.add(new Neraca(2,new String[]{"Sub 1","Sub 2","Sub 3","Sub 4","Sub 5","Sub 6"}));
                    JSONObject jo = new JSONObject(response);
                    //JSONArray jObj = new JSONArray(response);
                    JSONArray jObj = jo.getJSONArray("table");
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);
                        neraca.add(new Neraca(i,object.getString("NO1"),object.getString("URAIAN1"),object.getString("ANOMINAL1"),object.getString("NO2"),object.getString("URAIAN2"),object.getString("ANOMINAL2")));
                    }
                    JSONObject jTot = jo.getJSONObject("total");
                    ArrayList<String> total = new ArrayList<>();
                    total.add("Total Debit");
                    total.add(jTot.getString("TOTALDEBET"));
                    total.add("Total Kredit");
                    total.add(jTot.getString("TOTALKREDIT"));
                    neraca.add(new Neraca(total,Neraca.TYPE_TOTAL));
                    //lyku.setVisibility(View.GONE);
                    //lyjenis.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
                showSnackbar();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void laporanCashFlow(final String uid,final String cif) {

        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.LCFG +"?kode=Proses&uid="+uid+"&cif="+cif, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {

                    ArrayList<String> header = new ArrayList<>();
                    header.add("Tanggal");
                    header.add("Deskripsi");
                    header.add("Debit");
                    header.add("Kredit");
                    header.add("Saldo");
                    cFlow.add(new CFlow(header,CFlow.TYPE_HEADER));
                    JSONObject jo = new JSONObject(response);
                    JSONArray jObj = jo.getJSONArray("table");
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);
                        cFlow.add(new CFlow(i,object.getString("TANGGAL"),object.getString("KETERANGAN"),object.getString("DEBET"),object.getString("KREDIT"),object.getString("SALDO")));

                    }
                    adapterCashFlow.notifyDataSetChanged();
                    //rv.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
                showSnackbar();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void laporanPerubahanModal(final String uid,final String cif) {
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.LPMG+"?kode=Proses&uid="+uid+"&cif="+cif, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    pModal.clear();
                    ArrayList<String> sheader3 = new ArrayList<>();
                    sheader3.add("Keterangan");
                    sheader3.add("Perubahan");
                    sheader3.add("Saldo Akhir");

                    pModal.add(new PModal(sheader3,PModal.TYPE_HEADER));

                    JSONObject jo = new JSONObject(response);
                    JSONArray jObj = jo.getJSONArray("table");
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);
                        pModal.add(new PModal(i,object.getString("URAIAN"),object.getString("NOMINAL1"),object.getString("NOMINAL2")));
                    }
                    adapterPerubahanModal.notifyDataSetChanged();



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
                showSnackbar();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }


    private void fetchJenis() {
        String tag_string_req = "req_login";
        String endpoint;
        final String hint;
        if(pos == 0){
            endpoint = AppConfig.KOLEKTABLITAS + "?kode=ItemJenis&cif="+session.getUID();
            hint = "-- Jenis --";
        }else{
            endpoint = AppConfig.LK + "?kode=ItemCTG";
            hint = "-- Pilih Katagori --";
        }

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,endpoint , new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    JSONArray jObj = new JSONArray(response);
                    dataJenis.add(new SpinnerData("0",hint));
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);
                        if(pos == 1){
                            dataJenis.add(new SpinnerData(object.getString("acc"),object.getString("keterangan")));

                        }else{
                            dataJenis.add(new SpinnerData(object.getString("CTG"),object.getString("KATEGORI")));

                        }

                    }

                    jenis.notifyDataSetChanged();



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("login", "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);



    }

    private void fetchKU(final String id,final String cif,final String uid) {
        String tag_string_req = "req_login";
        String endpoint;
        final String hint;
        if(pos == 1){
            hint = "-- Pilih Kolektabilitas --";
            endpoint = AppConfig.KOLEKTABLITAS +"?kode=Item&cif="+cif;
        }else{
            hint = "-- Pilih Aspek --";
            endpoint = AppConfig.LK +"?kode=Item&uid="+uid+"&cif="+cif+"&ctg="+id;
        }
        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,endpoint, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    JSONArray jObj = new JSONArray(response);
                    dataKu.clear();
                    dataKu.add(new SpinnerData("0",hint));
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);
                        if(pos == 1){
                            dataKu.add(new SpinnerData(object.getString("URUT"),object.getString("COLLECTIBILITAS")));
                        }else{
                            dataKu.add(new SpinnerData(object.getString("KODE"),object.getString("ASPEK")));
                        }

                    }
                    ku.notifyDataSetChanged();
                    lyku.setVisibility(View.VISIBLE);


                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("login", "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showNoInternet(isConnected);
    }

    private void showNoInternet(boolean status) {
        final Snackbar snackbar = Snackbar.make(cl,"Tidak Ada Konektifitas",Snackbar.LENGTH_INDEFINITE)
                .setAction("Aktifkan", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setAction(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS);
                        startActivity(intent);
                    }
                });

        if(!status){
            snackbar.show();
        }else{
            snackbar.dismiss();
        }


    }
}
