package telkom.com.e_kopbkenew.Data;

/**
 * Created by sukri on 24/10/16.
 */

public class HomeMenu {
    private int image;
    private String title,subtitle;
    private boolean NULL;

    public HomeMenu(boolean NULL) {
        this.NULL = NULL;
    }

    public HomeMenu(int image, String title, String subtitle) {
        this.image = image;
        this.title = title;
        this.subtitle = subtitle;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public boolean isNULL() {
        return NULL;
    }

    public void setNULL(boolean NULL) {
        this.NULL = NULL;
    }
}
