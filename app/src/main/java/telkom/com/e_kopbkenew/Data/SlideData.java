package telkom.com.e_kopbkenew.Data;

/**
 * Created by sukri on 27/10/16.
 */

public class SlideData {
    int id;
    String Description;
    String Url;

    public SlideData(int id, String description, String url) {
        this.id = id;
        Description = description;
        Url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }
}
