package telkom.com.e_kopbkenew.Data;

import java.util.ArrayList;

/**
 * Created by sukri on 27/10/16.
 */

public class Neraca {

    public static final int TYPE_HEADER = 1;
    public static final int TYPE_SUBHEADER = 2;
    public static final int TYPE_CONTENT = 3;
    public static final int TYPE_TOTAL = 4;

    public Neraca() {
    }



    private int id;
    private String acc1,acc2,deskripsi1,deskripsi2,saldo1,saldo2;
    private ArrayList<String> header;

    private int viewType = 3;

    public Neraca(ArrayList<String> subheader, int viewType) {
        this.header = subheader;
        this.viewType = viewType;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public ArrayList<String> getHeader() {
        return header;
    }

    public void setHeader(ArrayList<String> header) {
        this.header = header;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAcc1() {
        if(acc1.equals("null")){
            return "";
        }
        return acc1;
    }

    public void setAcc1(String acc1) {
        this.acc1 = acc1;
    }

    public String getAcc2() {
        if(acc2.equals("null")){
            return "";
        }
        return acc2;
    }

    public void setAcc2(String acc2) {
        this.acc2 = acc2;
    }

    public String getDeskripsi1() {
        if(deskripsi1.equals("null")){
            return "";
        }
        return deskripsi1;
    }

    public void setDeskripsi1(String deskripsi1) {
        this.deskripsi1 = deskripsi1;
    }

    public String getDeskripsi2() {
        if(deskripsi2.equals("null")){
            return "";
        }
        return deskripsi2;
    }

    public void setDeskripsi2(String deskripsi2) {
        this.deskripsi2 = deskripsi2;
    }

    public String getSaldo1() {
        if(saldo1.equals("null")){
            return "";
        }
        return saldo1;
    }

    public void setSaldo1(String saldo1) {
        this.saldo1 = saldo1;
    }

    public String getSaldo2() {
        if(saldo2.equals("null")){
            return "";
        }
        return saldo2;
    }

    public void setSaldo2(String saldo2) {
        this.saldo2 = saldo2;
    }

    public Neraca(int id, String acc1, String deskripsi1, String saldo1, String acc2, String deskripsi2, String saldo2) {
        this.id = id;
        this.acc1 = acc1;
        this.acc2 = acc2;
        this.deskripsi1 = deskripsi1;
        this.deskripsi2 = deskripsi2;
        this.saldo1 = saldo1;
        this.saldo2 = saldo2;
    }
}
