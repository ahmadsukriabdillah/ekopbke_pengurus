package telkom.com.e_kopbkenew.Data;

import java.util.ArrayList;

/**
 * Created by sukri on 27/10/16.
 */

public class NeracaLajur {

    public static final int TYPE_HEADER = 1;
    public static final int TYPE_SUBHEADER = 2;
    public static final int TYPE_CONTENT = 3;
    public static final int TYPE_TOTAL = 4;

    public NeracaLajur() {

    }

    private int id;
    private String acc,deskripsi,saad,saak,md,mk,sad,sak,lb,lp,na,nke;
    private ArrayList<String> header;

    private int viewType = 3;


    public NeracaLajur(ArrayList<String> subheader, int viewType) {
        this.header = subheader;
        this.viewType = viewType;
    }

    public NeracaLajur(int id, String acc, String deskripsi, String saad, String saak, String md, String mk, String sad, String sak, String lb, String lp, String na, String nke) {
        this.id = id;
        this.acc = acc;
        this.deskripsi = deskripsi;
        this.saad = saad;
        this.saak = saak;
        this.md = md;
        this.mk = mk;
        this.sad = sad;
        this.sak = sak;
        this.lb = lb;
        this.lp = lp;
        this.na = na;
        this.nke = nke;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAcc() {
        return acc;
    }

    public void setAcc(String acc) {
        this.acc = acc;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getSaad() {
        return saad;
    }

    public void setSaad(String saad) {
        this.saad = saad;
    }

    public String getSaak() {
        return saak;
    }

    public void setSaak(String saak) {
        this.saak = saak;
    }

    public String getMd() {
        return md;
    }

    public void setMd(String md) {
        this.md = md;
    }

    public String getMk() {
        return mk;
    }

    public void setMk(String mk) {
        this.mk = mk;
    }

    public String getSad() {
        return sad;
    }

    public void setSad(String sad) {
        this.sad = sad;
    }

    public String getSak() {
        return sak;
    }

    public void setSak(String sak) {
        this.sak = sak;
    }

    public String getLb() {
        return lb;
    }

    public void setLb(String lb) {
        this.lb = lb;
    }

    public String getLp() {
        return lp;
    }

    public void setLp(String lp) {
        this.lp = lp;
    }

    public String getNa() {
        return na;
    }

    public void setNa(String na) {
        this.na = na;
    }

    public String getNke() {
        return nke;
    }

    public void setNke(String nke) {
        this.nke = nke;
    }

    public ArrayList<String> getHeader() {
        return header;
    }

    public void setHeader(ArrayList<String> header) {
        this.header = header;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }
}
