package telkom.com.e_kopbkenew.Data;

/**
 * Created by sukri on 24/10/16.
 */

public class Profile {

    private String judul;
    private String keterangan;

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Profile(String judul, String keterangan) {
        this.judul = judul;
        this.keterangan = keterangan;
    }
}
