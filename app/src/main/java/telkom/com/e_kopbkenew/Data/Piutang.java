package telkom.com.e_kopbkenew.Data;

import java.util.ArrayList;

/**
 * Created by sukri on 27/10/16.
 */

public class Piutang {

    public static final int TYPE_HEADER = 1;
    public static final int TYPE_SUBHEADER = 2;
    public static final int TYPE_CONTENT = 3;
    public static final int TYPE_TOTAL = 4;

    public Piutang() {

    }



    private int id;
    private String content1,content2,content3,content4,content5,content6,content7;
    private ArrayList<String> header;

    private int viewType = 3;

    public Piutang(ArrayList<String> subheader, int viewType) {
        this.header = subheader;
        this.viewType = viewType;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public ArrayList<String> getHeader() {
        return header;
    }

    public void setHeader(ArrayList<String> header) {
        this.header = header;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Piutang(int id, String content1, String content2, String content3, String content4, String content5, String content6, String content7) {
        this.id = id;
        this.content1 = content1;
        this.content2 = content2;
        this.content3 = content3;
        this.content4 = content4;
        this.content5 = content5;
        this.content6 = content6;
        this.content7 = content7;
    }

    public String getContent1() {
        return content1;
    }

    public void setContent1(String content1) {
        this.content1 = content1;
    }

    public String getContent2() {
        return content2;
    }

    public void setContent2(String content2) {
        this.content2 = content2;
    }

    public String getContent3() {
        return content3;
    }

    public void setContent3(String content3) {
        this.content3 = content3;
    }

    public String getContent4() {
        return content4;
    }

    public void setContent4(String content4) {
        this.content4 = content4;
    }

    public String getContent5() {
        return content5;
    }

    public void setContent5(String content5) {
        this.content5 = content5;
    }

    public String getContent6() {
        return content6;
    }

    public void setContent6(String content6) {
        this.content6 = content6;
    }

    public String getContent7() {
        return content7;
    }

    public void setContent7(String content7) {
        this.content7 = content7;
    }
}
