package telkom.com.e_kopbkenew.Data;

import java.util.ArrayList;

/**
 * Created by sukri on 03/11/16.
 */

public class AllData {
    public static final int TYPE_HEADER = 1;
    public static final int TYPE_SUBHEADER = 2;
    public static final int TYPE_CONTENT = 3;
    public static final int TYPE_TOTAL = 4;

    private int id;
    private ArrayList<String> header;

    private int viewType = 3;

    public AllData(ArrayList<String> subheader, int viewType) {
        this.header = subheader;
        this.viewType = viewType;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public ArrayList<String> getHeader() {
        return header;
    }

    public void setHeader(ArrayList<String> header) {
        this.header = header;
    }


}
