package telkom.com.e_kopbkenew.Data;

import java.util.ArrayList;

/**
 * Created by sukri on 27/10/16.
 */

public class PModal {

    public static final int TYPE_HEADER = 1;
    public static final int TYPE_CONTENT = 2;

    public PModal() {
    }



    private int id;
    private String keterangan,perubahan,saldo;
    private ArrayList<String> header;

    private int viewType = 2;

    public PModal(ArrayList<String> subheader, int viewType) {
        this.header = subheader;
        this.viewType = viewType;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public ArrayList<String> getHeader() {
        return header;
    }

    public void setHeader(ArrayList<String> header) {
        this.header = header;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PModal(int id, String keterangan, String perubahan, String saldo) {
        this.id = id;
        this.keterangan = keterangan;
        this.perubahan = perubahan;
        this.saldo = saldo;
    }

    public String getPerubahan() {
        return perubahan;
    }

    public void setPerubahan(String perubahan) {
        this.perubahan = perubahan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }


    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }
}
