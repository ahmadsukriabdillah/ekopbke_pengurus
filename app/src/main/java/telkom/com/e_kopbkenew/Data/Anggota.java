package telkom.com.e_kopbkenew.Data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sukri on 02/11/16.
 */
public class Anggota implements Parcelable {
    private String CIB,NIP,NAMA,KI,NAMA_INSTANSI,ID,NOID,TGL_EXPIRED,ALAMAT;

    public Anggota(String CIB, String NIP, String NAMA, String KI, String NAMA_INSTANSI, String ID, String NOID, String TGL_EXPIRED, String ALAMAT) {
        this.CIB = CIB;
        this.NIP = NIP;
        this.NAMA = NAMA;
        this.KI = KI;
        this.NAMA_INSTANSI = NAMA_INSTANSI;
        this.ID = ID;
        this.NOID = NOID;
        this.TGL_EXPIRED = TGL_EXPIRED;
        this.ALAMAT = ALAMAT;
    }

    public String getCIB() {
        return CIB;
    }

    public void setCIB(String CIB) {
        this.CIB = CIB;
    }

    public String getNIP() {
        return NIP;
    }

    public void setNIP(String NIP) {
        this.NIP = NIP;
    }

    public String getNAMA() {
        return NAMA;
    }

    public void setNAMA(String NAMA) {
        this.NAMA = NAMA;
    }

    public String getKI() {
        return KI;
    }

    public void setKI(String KI) {
        this.KI = KI;
    }

    public String getNAMA_INSTANSI() {
        return NAMA_INSTANSI;
    }

    public void setNAMA_INSTANSI(String NAMA_INSTANSI) {
        this.NAMA_INSTANSI = NAMA_INSTANSI;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getNOID() {
        return NOID;
    }

    public void setNOID(String NOID) {
        this.NOID = NOID;
    }

    public String getTGL_EXPIRED() {
        return TGL_EXPIRED;
    }

    public void setTGL_EXPIRED(String TGL_EXPIRED) {
        this.TGL_EXPIRED = TGL_EXPIRED;
    }

    public String getALAMAT() {
        return ALAMAT;
    }

    public void setALAMAT(String ALAMAT) {
        this.ALAMAT = ALAMAT;
    }

    public Anggota() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.CIB);
        dest.writeString(this.NIP);
        dest.writeString(this.NAMA);
        dest.writeString(this.KI);
        dest.writeString(this.NAMA_INSTANSI);
        dest.writeString(this.ID);
        dest.writeString(this.NOID);
        dest.writeString(this.TGL_EXPIRED);
        dest.writeString(this.ALAMAT);
    }

    protected Anggota(Parcel in) {
        this.CIB = in.readString();
        this.NIP = in.readString();
        this.NAMA = in.readString();
        this.KI = in.readString();
        this.NAMA_INSTANSI = in.readString();
        this.ID = in.readString();
        this.NOID = in.readString();
        this.TGL_EXPIRED = in.readString();
        this.ALAMAT = in.readString();
    }

    public static final Parcelable.Creator<Anggota> CREATOR = new Parcelable.Creator<Anggota>() {
        @Override
        public Anggota createFromParcel(Parcel source) {
            return new Anggota(source);
        }

        @Override
        public Anggota[] newArray(int size) {
            return new Anggota[size];
        }
    };
}
