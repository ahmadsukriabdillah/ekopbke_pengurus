package telkom.com.e_kopbkenew.Data;

import java.util.ArrayList;

/**
 * Created by sukri on 27/10/16.
 */

public class CFlow {

    public static final int TYPE_HEADER = 1;
    public static final int TYPE_CONTENT = 2;

    public CFlow() {
    }



    private int id;
    private String tgl,keterangan,debet,kredit,saldo;
    private ArrayList<String> header;

    private int viewType = 2;

    public CFlow(ArrayList<String> subheader, int viewType) {
        this.header = subheader;
        this.viewType = viewType;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public ArrayList<String> getHeader() {
        return header;
    }

    public void setHeader(ArrayList<String> header) {
        this.header = header;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CFlow(int id, String tgl, String keterangan, String debet, String kredit, String saldo) {
        this.id = id;
        this.tgl = tgl;
        this.keterangan = keterangan;
        this.debet = debet;
        this.kredit = kredit;
        this.saldo = saldo;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getDebet() {
        return debet;
    }

    public void setDebet(String debet) {
        this.debet = debet;
    }

    public String getKredit() {
        return kredit;
    }

    public void setKredit(String kredit) {
        this.kredit = kredit;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }
}
