package telkom.com.e_kopbkenew.Data;

/**
 * Created by sukri on 27/10/16.
 */

public class SpinnerData {

    String id;
    String name;

    public SpinnerData(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.getName();
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
