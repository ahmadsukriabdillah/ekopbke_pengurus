package telkom.com.e_kopbkenew;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.IdRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ncapdevi.fragnav.FragNavController;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabClickListener;

import java.util.ArrayList;
import java.util.List;

import telkom.com.e_kopbkenew.Fragment.BaseFragment;
import telkom.com.e_kopbkenew.Fragment.FragmentProfile;
import telkom.com.e_kopbkenew.Fragment.Home;
import telkom.com.e_kopbkenew.Fragment.Password;
import telkom.com.e_kopbkenew.Helper.AppController;
import telkom.com.e_kopbkenew.Helper.Connection;
import telkom.com.e_kopbkenew.Helper.SessionManager;

public class MainActivity extends AppCompatActivity implements BaseFragment.FragmentNavigation,Home.NavigateView,Connection.ConnectivityReceiverListener {
    private BottomBar mBottomBar;
    private FragNavController fragNavController;
    private Toolbar toolbar;

    //indices to fragments
    private final int TAB_FIRST = FragNavController.TAB1;
    private final int TAB_SECOND = FragNavController.TAB2;
    private final int TAB_THIRD = FragNavController.TAB3;
    private CoordinatorLayout cl;
    private SessionManager session;

    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        cl = (CoordinatorLayout) findViewById(R.id.cl);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        pDialog = new ProgressDialog(this);

        List<Fragment> fragments = new ArrayList<>(3);

        fragments.add(Home.newInstance(0));
        fragments.add(FragmentProfile.newInstance(0));
        fragments.add(Password.newInstance(0));

        fragNavController = new FragNavController(getSupportFragmentManager(),R.id.content,fragments);

        session = new SessionManager(this);
        if (session.isLoggedIn()) {
            toolbar.setTitle(R.string.app_name);

        }else{
            startActivity(new Intent(this, HalamanLogin.class));
            finish();
        }

        mBottomBar = BottomBar.attach(this, savedInstanceState);
        //mBottomBar = (BottomBar) findViewById(R.id.bottomBar);
        mBottomBar.setItems(R.menu.menu_bottom_bar);
        mBottomBar.hideShadow();
        mBottomBar.setOnMenuTabClickListener(new OnMenuTabClickListener() {
            @Override
            public void onMenuTabSelected(@IdRes int menuItemId) {
                //switch between tabs
                switch (menuItemId) {
                    case R.id.bottomBarItemOne:
                        fragNavController.switchTab(TAB_FIRST);
                        //toolbar.setTitle(R.string.app_name);
                        break;
                    case R.id.bottomBarItemSecond:
                        fragNavController.switchTab(TAB_SECOND);
                        //toolbar.setTitle("Profile");
                        break;
                    case R.id.bottomBarItemThird:
                        fragNavController.switchTab(TAB_THIRD);
                        //toolbar.setTitle("Ubah Password");
                        //onCreateOptionsMenu(getMenuInflater().inflate(R.menu.menu_profile,menu);)
                        break;
                    case R.id.bottomBarItemForth:

                        konfirmasi_keluar(true);
                        break;
                }
            }

            @Override
            public void onMenuTabReSelected(@IdRes int menuItemId) {
                if (menuItemId == R.id.bottomBarItemOne) {
                    fragNavController.clearStack();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        //Toast.makeText(this," "+fragNavController.getCurrentStack().size(),Toast.LENGTH_LONG).show();
        if (fragNavController.getCurrentStack().size() > 1) {
            fragNavController.pop();
        } else {
            konfirmasi_keluar(false);
            //super.onBackPressed();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        mBottomBar.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == R.id.action_ganti_user) {
            final AlertDialog alert = new AlertDialog.Builder(MainActivity.this).create();
            alert.setTitle("Peringantan");

            alert.setMessage("Apakah anda ingin Keluar?");
            alert.setButton(AlertDialog.BUTTON_POSITIVE, "YA",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            //super.onBackPressed();
                            session.setLogin(false);
                            startActivity(new Intent(getApplicationContext(), HalamanLogin.class));
                            finish();
                        }
                    });

            alert.setButton(AlertDialog.BUTTON_NEGATIVE, "TIDAK",
                    new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            alert.dismiss();
                        }
                    });


            alert.show();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



    private void konfirmasi_keluar(final Boolean a) {
        final AlertDialog alert = new AlertDialog.Builder(MainActivity.this).create();
        alert.setTitle("Peringantan");

        alert.setMessage("Apakah anda ingin Keluar?");
        alert.setButton(AlertDialog.BUTTON_POSITIVE, "YA",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        //super.onBackPressed();
                        session.setLogin(false);
                        finishAffinity();
                    }
                });

        alert.setButton(AlertDialog.BUTTON_NEGATIVE, "TIDAK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        if(a){
                            alert.dismiss();
                            mBottomBar.selectTabAtPosition(0,true);
                        }else{
                            alert.dismiss();

                        }

                    }
                });


        alert.show();
    }

    @Override
    public void pushFragment(Fragment fragment) {
        fragNavController.push(fragment);
    }

    @Override
    public void switchTab(int index, boolean anim) {
        mBottomBar.selectTabAtPosition(index,anim);
    }


    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showNoInternet(isConnected);
    }

    private void showNoInternet(boolean status) {
        final Snackbar snackbar = Snackbar.make(cl,"Tidak Ada Konektifitas",Snackbar.LENGTH_INDEFINITE)
                .setAction("Aktifkan", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setAction(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS);
                        startActivity(intent);
                    }
                });

        if(!status){
            snackbar.show();
        }else{
            snackbar.dismiss();
        }


    }
}