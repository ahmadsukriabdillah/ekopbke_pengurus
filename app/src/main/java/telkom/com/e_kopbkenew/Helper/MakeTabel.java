package telkom.com.e_kopbkenew.Helper;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import telkom.com.e_kopbkenew.R;

/**
 * Created by sukri on 02/11/16.
 */

public class MakeTabel {
    public static TextView makeHeader(Context context, String text, int width){
        TextView tv = new TextView(context);
        tv.setText(text);
        tv.setBackgroundResource(R.drawable.header_background);
        tv.setGravity(View.TEXT_ALIGNMENT_GRAVITY);
        return tv;
    }
}
