package telkom.com.e_kopbkenew.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SessionManager {
	// LogCat tag
	private static String TAG = SessionManager.class.getSimpleName();

	// Shared Preferences
	SharedPreferences pref;

	Editor editor;
	Context _context;

	// Shared pref mode
	int PRIVATE_MODE = 0;

	// Shared preferences file name
	private static final String PREF_NAME = "ekopbkenew";

	private static final String KEY_IS_Login = "isLoggedInAdmin";

	private static final String key_nama = "nama";
	private static final String key_nama_client = "namaclient";
	private static final String key_cif = "cif";



	public SessionManager(Context context) {
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	public void setLogin(boolean admin) {

		editor.putBoolean(KEY_IS_Login, admin);

		// commit changes
		editor.commit();

		Log.d(TAG, "User login session modified!");
	}
	public void setID(String nama,String cif,String namaclient) {

		editor.putString(key_cif, cif);
		editor.putString(key_nama, nama);
		editor.putString(key_nama_client,namaclient);
		editor.commit();

		Log.d(TAG, "User ID session modified!");
	}


	
	public boolean isLoggedIn(){
		return pref.getBoolean(KEY_IS_Login, false);
	}
	public String getUID(){
		return pref.getString(key_cif, "");
	}
	public String getNAMA(){
		return pref.getString(key_nama, "");
	}
	public String getNAMAClient(){
		return pref.getString(key_nama_client, "");
	}

}
