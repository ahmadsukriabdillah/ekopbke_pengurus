package telkom.com.e_kopbkenew.Helper;

/**
 * Created by sukri on 22/10/16.
 */

public class AppConfig {

    public static final String SERVER = "http://e-kopbke.com/ekop-mobile/Server/";
    public static final String LOGIN = SERVER + "Login.php";
    public static final String SUMMARY = SERVER + "Summary.php";

    public static final String PROFILE = SERVER + "Cif.php?kode=Item&cif=";
    public static final String JENIS = SERVER + "Daftar_UnitBisnis.php?kode=ItemJenis";
    public static final String KODEUNIT = SERVER + "Daftar_UnitBisnis.php?kode=Item&cif=";
    public static final String SLIDE = SERVER + "Slide.php";
    public static final String CHANGE_PASSWORD = SERVER + "Password.php";

    //public static final String LAPORAN_NERACA = SERVER + "LaporanNeraca.php";
    public static final String LAPORAN_NERACA = SERVER + "Neraca.php";
    public static final String LAPORAN_NERACA_LAJUR = SERVER + "NeracaLajurGabungan.php";
    public static final String LAPORAN_LABA_RUGI = SERVER + "LabaRugi.php";
    public static final String KOMPARASi = SERVER + "Komparasi.php";
    public static final String CASH_FLOW = SERVER + "CashFlow.php";
    public static final String LAPORAN_PERUBAHAN_MODAL = SERVER + "PerubahanModal.php";
    public static final String LIST_ANGGOTA = SERVER + "Anggota.php";

    /**
     * LAPORANG MASTER SALDO
     *
     */
    public static final String M_SIM_JENIS = SERVER + "MasterSimp.php";
    public static final String M_DEP_JENIS = SERVER + "MasterDeposito.php";
    public static final String M_HUT_JENIS = SERVER + "MasterHutang.php";
    public static final String M_PIU_JENIS = SERVER + "MasterPiutang.php";


    /**
     *  LAPORANG GABUNGAN
     */

    public static final String DIRECTORY_GABUNGAN = "Gabungan/";
    public static final String LCFG = SERVER + DIRECTORY_GABUNGAN + "LCFG.php";
    public static final String LPMG = SERVER + DIRECTORY_GABUNGAN + "LPMG.php";
    public static final String LNG = SERVER + DIRECTORY_GABUNGAN + "LNG.php";
    public static final String LRB = SERVER + DIRECTORY_GABUNGAN + "LRB.php";
    public static final String NLG = SERVER + DIRECTORY_GABUNGAN + "NLG.php";
    public static final String LK = SERVER + DIRECTORY_GABUNGAN + "LK.php";
    public static final String LP = SERVER + DIRECTORY_GABUNGAN + "LP.php";
    public static final String KOLEKTABLITAS = SERVER + DIRECTORY_GABUNGAN + "Kolektablitas.php" ;
}
