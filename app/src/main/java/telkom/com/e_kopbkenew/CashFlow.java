package telkom.com.e_kopbkenew;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowInsets;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import telkom.com.e_kopbkenew.Adapter.AdapterCashFlow;
import telkom.com.e_kopbkenew.Data.CFlow;
import telkom.com.e_kopbkenew.Data.SpinnerData;
import telkom.com.e_kopbkenew.Helper.AppConfig;
import telkom.com.e_kopbkenew.Helper.AppController;
import telkom.com.e_kopbkenew.Helper.Connection;
import telkom.com.e_kopbkenew.Helper.SessionManager;

public class CashFlow extends AppCompatActivity implements DatePickerDialog.OnDateSetListener,Connection.ConnectivityReceiverListener {

    private EditText dateStart,dateEnd;
    private float mx, my;
    private float curX, curY;
    private static int state;
    private Spinner spinerJenis,spinerKodeUnit;
    private SessionManager session;
    private ArrayList<SpinnerData> dataJenis;
    private ArrayList<SpinnerData> dataKu;
    private ProgressDialog pDialog;
    private CoordinatorLayout cl;
    private RelativeLayout layouttanggal;
    private LinearLayout layoutjenis,layoutkodeunit;
    private ArrayAdapter<SpinnerData> jenis;
    private ArrayAdapter<SpinnerData> ku;
    private AdapterCashFlow adapter;
    private ArrayList<CFlow> cflow;
    private RecyclerView rv;
    private Toolbar toolbar;
    private ScrollView vScroll;
    private HorizontalScrollView hScroll;
    private float scale = 1f;
    private ScaleGestureDetector detector;
    GestureDetector gestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_flow);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        cflow = new ArrayList<>();
        rv = (RecyclerView) findViewById(R.id.rv);
        dateEnd = (EditText) findViewById(R.id.dateend);
        dateStart = (EditText) findViewById(R.id.datestart);
        session = new SessionManager(this);
        dataJenis = new ArrayList<>();
        cl = (CoordinatorLayout) findViewById(R.id.cl);
        dataKu = new ArrayList<>();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        spinerJenis = (Spinner) findViewById(R.id.spinner2);
        spinerKodeUnit = (Spinner) findViewById(R.id.spinner3);
        layouttanggal = (RelativeLayout) findViewById(R.id.tglgrup);
        layoutjenis = (LinearLayout) findViewById(R.id.jenis);
        layoutkodeunit = (LinearLayout) findViewById(R.id.kodeunit);
        rv = (RecyclerView) findViewById(R.id.rv);
        vScroll = (ScrollView)findViewById(R.id.vScroll);
        hScroll = (HorizontalScrollView) findViewById(R.id.hScroll);
        //rv.setScaleX(0.3f);
        //rv.setScaleX(0.3f);
        //gestureDetector = new GestureDetector(this, new GestureListener());
        detector = new ScaleGestureDetector(this,new ScaleListener());


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        adapter = new AdapterCashFlow(this,cflow);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setAutoMeasureEnabled(true);

        rv.setLayoutManager(mLayoutManager);

        rv.setAdapter(adapter);
        dateEnd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(dateStart.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(),"Pilih Tanggal Mulai terlebih Dahulu.",Toast.LENGTH_LONG).show();
                }else{
                    panggilDialogDate(2);
                }

            }
        });

        dateStart.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                panggilDialogDate(1);
            }
        });

        /*dateEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                panggilDialogDate(2);
            }
        });*/

        jenis = new ArrayAdapter<>(this,R.layout.spinner,dataJenis);
        jenis.setDropDownViewResource(R.layout.spinner); // The drop down view
        spinerJenis.setAdapter(jenis);

        spinerJenis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0){

                }else{
                    layoutkodeunit.setVisibility(View.VISIBLE);
                    fetchKU(dataJenis.get(i).getId(),session.getUID());

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ku = new ArrayAdapter<>(this,R.layout.spinner,dataKu);
        ku.setDropDownViewResource(R.layout.spinner); // The drop down view
        spinerKodeUnit.setAdapter(ku);

        spinerKodeUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0){

                }else {
                    layouttanggal.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        fetchJenis();


    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        super.dispatchTouchEvent(ev);
        //mScaleDetector.onTouchEvent(ev);
        //gestureDetector.onTouchEvent(ev);
        detector.onTouchEvent(ev);
        switch (ev.getAction()) {

            case MotionEvent.ACTION_DOWN:
                mx = ev.getX();
                my = ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                curX = ev.getX();
                curY = ev.getY();
                vScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                hScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                mx = curX;
                my = curY;
                break;
            case MotionEvent.ACTION_UP:
                curX = ev.getX();
                curY = ev.getY();
                vScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                hScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                break;
        }

        return true;
    }

    private void panggilDialogDate(int i) {
        state = i;
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                CashFlow.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }




    private void loadChasFlow(final String uid,final String ku,final String cif,final String dateStart,final String dateEnd) {

        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.CASH_FLOW +"?uid="+uid+"&ku="+ku+"&cif="+cif+"&tanggal="+dateStart+"&tanggalsd="+dateEnd, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    cflow.clear();

                    ArrayList<String> header = new ArrayList<>();
                    header.add("Tanggal");
                    header.add("Deskripsi");
                    header.add("Debit");
                    header.add("Kredit");
                    header.add("Saldo");
                    cflow.add(new CFlow(header,CFlow.TYPE_HEADER));
                    JSONArray jObj = new JSONArray(response);
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);
                        cflow.add(new CFlow(i,object.getString("TANGGAL"),object.getString("KETERANGAN"),object.getString("DEBET"),object.getString("KREDIT"),object.getString("SALDO")));

                    }
                    adapter.notifyDataSetChanged();
                    rv.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("login", "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void fetchJenis() {
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.JENIS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    JSONArray jObj = new JSONArray(response);
                    dataJenis.add(new SpinnerData("0","-- Pilih Jenis Usaha --"));
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);
                        dataJenis.add(new SpinnerData(object.getString("ID"),object.getString("JENIS")));

                    }
                    jenis.notifyDataSetChanged();



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("login", "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);



    }

    private void fetchKU(final String id,final String cif) {
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.KODEUNIT+cif+"&jenis="+id, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    JSONArray jObj = new JSONArray(response);
                    dataKu.clear();
                    dataKu.add(new SpinnerData("0","-- Pilih Nama Unit --"));
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);
                        dataKu.add(new SpinnerData(object.getString("ku"),object.getString("nama_unit")));

                    }
                    ku.notifyDataSetChanged();


                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("login", "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        if(state == 1){
            dateStart.setText(year+"/"+monthOfYear+"/"+dayOfMonth);
        }else if(state == 2){
            dateEnd.setText(year+"/"+monthOfYear+"/"+dayOfMonth);
            loadChasFlow(session.getNAMA(),dataKu.get(spinerKodeUnit.getSelectedItemPosition()).getId(),session.getUID(),dateStart.getText().toString(),dateEnd.getText().toString());

        }else{

        }

    }


    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {


        float onScaleBegin = 0;
        float onScaleEnd = 0;

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scale *= detector.getScaleFactor();
            rv.setScaleX(scale);
            rv.setScaleY(scale);
            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {

            //Toast.makeText(getApplicationContext(),"Scale Begin" ,Toast.LENGTH_SHORT).show();
            onScaleBegin = scale;

            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {

            //Toast.makeText(getApplicationContext(),"Scale Ended",Toast.LENGTH_SHORT).show();
            onScaleEnd = scale;

            if (onScaleEnd > onScaleBegin){
                //  Toast.makeText(getApplicationContext(),"Scaled Up by a factor of  " + String.valueOf( onScaleEnd / onScaleBegin ), Toast.LENGTH_SHORT  ).show();
            }

            if (onScaleEnd < onScaleBegin){
                //Toast.makeText(getApplicationContext(),"Scaled Down by a factor of  " + String.valueOf( onScaleBegin / onScaleEnd ), Toast.LENGTH_SHORT  ).show();
            }

            super.onScaleEnd(detector);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showNoInternet(isConnected);
    }

    private void showNoInternet(boolean status) {
        final Snackbar snackbar = Snackbar.make(cl,"Tidak Ada Konektifitas",Snackbar.LENGTH_INDEFINITE)
                .setAction("Aktifkan", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setAction(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS);
                        startActivity(intent);
                    }
                });

        if(!status){
            snackbar.show();
        }else{
            snackbar.dismiss();
        }


    }
}
