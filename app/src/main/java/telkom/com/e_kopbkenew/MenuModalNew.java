package telkom.com.e_kopbkenew;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import telkom.com.e_kopbkenew.Adapter.AdapterDeposito;
import telkom.com.e_kopbkenew.Adapter.AdapterPiutang;
import telkom.com.e_kopbkenew.Adapter.AdapterSaldoPinjaman;
import telkom.com.e_kopbkenew.Adapter.HomeMenuAdapter;
import telkom.com.e_kopbkenew.Data.Deposito;
import telkom.com.e_kopbkenew.Data.HomeMenu;
import telkom.com.e_kopbkenew.Data.Pinjaman;
import telkom.com.e_kopbkenew.Data.Piutang;
import telkom.com.e_kopbkenew.Data.SpinnerData;
import telkom.com.e_kopbkenew.Helper.AppConfig;
import telkom.com.e_kopbkenew.Helper.AppController;
import telkom.com.e_kopbkenew.Helper.Connection;
import telkom.com.e_kopbkenew.Helper.SessionManager;

public class MenuModalNew extends AppCompatActivity  implements Connection.ConnectivityReceiverListener{
    //menu
    private RecyclerView rv;
    private HomeMenuAdapter adapter;
    private ArrayList<HomeMenu> albumList;
    //rv list


    private RecyclerView rv2;
    private AdapterSaldoPinjaman adapterSimpanan;
    private ArrayList<Pinjaman> listSimpanan;

    //adapter deposito

    private AdapterDeposito adapterDeposito;
    private ArrayList<Deposito> listDeposito;

    private Spinner spinner;
    private ArrayList<SpinnerData> spinnerData;
    private ArrayAdapter<SpinnerData> adapterSpiner;
    private static int pil= 99;
    private SessionManager sesion;
    private ProgressDialog pDialog;
    private LinearLayout ll;
    private float mx, my;
    private float curX, curY;
    private ScrollView vScroll;
    private HorizontalScrollView hScroll;
    private float scale = 1f;
    private ScaleGestureDetector detector;
    private Toolbar toolbar;
    private CoordinatorLayout cl;
    private static String temp;
    private ArrayList<Piutang> listPiutang;
    private AdapterPiutang adapterPiutang;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_modal_new);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Master Saldo");
        setSupportActionBar(toolbar);
        vScroll = (ScrollView)findViewById(R.id.vScroll);
        cl = (CoordinatorLayout) findViewById(R.id.cl);
        hScroll = (HorizontalScrollView) findViewById(R.id.hScroll);
        //rv.setScaleX(0.3f);
        //rv.setScaleX(0.3f);
        //gestureDetector = new GestureDetector(this, new GestureListener());
        detector = new ScaleGestureDetector(this,new ScaleListener());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rv = (RecyclerView) findViewById(R.id.rv);
        rv2 = (RecyclerView) findViewById(R.id.rv2);
        ll = (LinearLayout) findViewById(R.id.jenis);
        ll.setVisibility(View.GONE);
        sesion = new SessionManager(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        spinner = (Spinner) findViewById(R.id.spinner2);
        spinnerData = new ArrayList<>();
        adapterSpiner = new ArrayAdapter<>(this,R.layout.spinner,spinnerData);
        spinner.setPrompt("-- Pilih Jenis --");
        spinner.setAdapter(adapterSpiner);

        albumList = new ArrayList<>();
        adapter = new HomeMenuAdapter(getApplicationContext(),albumList);
        RecyclerView.LayoutManager manager = new GridLayoutManager(getApplicationContext(),4);
        rv.setLayoutManager(manager);
        rv.setAdapter(adapter);
        albumList.add(new HomeMenu(R.drawable.simpanan,"Master Simpanan","1"));
        albumList.add(new HomeMenu(R.drawable.deposito,"Master Deposito","2"));
        albumList.add(new HomeMenu(R.drawable.hutang,"Master Hutang","3"));
        albumList.add(new HomeMenu(R.drawable.piutang,"Master Piutang","4"));
        adapter.notifyDataSetChanged();

        listSimpanan = new ArrayList<>();
        adapterSimpanan = new AdapterSaldoPinjaman(getApplicationContext(),listSimpanan);

        listDeposito = new ArrayList<>();
        adapterDeposito = new AdapterDeposito(getApplicationContext(),listDeposito);


        listPiutang = new ArrayList<>();
        adapterPiutang = new AdapterPiutang(getApplicationContext(),listPiutang);

        RecyclerView.LayoutManager manager2 = new LinearLayoutManager(getApplicationContext());
        rv2.setLayoutManager(manager2);


        //spinner

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0){

                }else{
                    switch (pil){
                        case 0:
                            fetchLaporanSimpanan(sesion.getNAMA(),sesion.getUID(),spinnerData.get(i).getId());
                            temp = spinnerData.get(i).getId();
                            break;
                        case 1:
                            fetchLaporanDeposito(sesion.getNAMA(),sesion.getUID(),spinnerData.get(i).getId());
                            temp = spinnerData.get(i).getId();
                            break;
                        case 2:
                            fetchLaporanHutang(sesion.getNAMA(),sesion.getUID(),spinnerData.get(i).getId());
                            temp = spinnerData.get(i).getId();
                            break;
                        case 3:
                            fetchLaporanPiutang(sesion.getNAMA(),sesion.getUID(),spinnerData.get(i).getId());
                            temp = spinnerData.get(i).getId();
                            break;

                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        rv.addOnItemTouchListener(new HomeMenuAdapter.RecyclerTouchListener(getApplicationContext(), rv, new HomeMenuAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                switch (position){

                    case 0:
                        toolbar.setTitle("Master Saldo - Simpanan");
                        setSupportActionBar(toolbar);
                        spinner.setSelection(0);

                        ll.setVisibility(View.VISIBLE);
                        fetchJenis(sesion.getNAMA(),sesion.getUID(),position);
                        pil = position;

                        listSimpanan.clear();
                        rv2.setAdapter(adapterSimpanan);
                        break;

                    case 1:
                        toolbar.setTitle("Master Saldo - Deposito");
                        setSupportActionBar(toolbar);
                        spinner.setSelection(0);

                        ll.setVisibility(View.VISIBLE);
                        fetchJenis(sesion.getNAMA(),sesion.getUID(),position);
                        pil = position;
                        listDeposito.clear();
                        rv2.setAdapter(adapterDeposito);

                        break;

                    case 2:
                        toolbar.setTitle("Master Saldo - Hutang");
                        setSupportActionBar(toolbar);
                        spinner.setSelection(0);

                        ll.setVisibility(View.VISIBLE);
                        fetchJenis(sesion.getNAMA(),sesion.getUID(),position);
                        pil = position;
                        listSimpanan.clear();
                        rv2.setAdapter(adapterSimpanan);
                        break;



                    case 3:
                        toolbar.setTitle("Master Saldo - Piutang");
                        setSupportActionBar(toolbar);
                        spinner.setSelection(0);

                        ll.setVisibility(View.VISIBLE);
                        fetchJenis(sesion.getNAMA(),sesion.getUID(),position);
                        pil = position;
                        listPiutang.clear();
                        rv2.setAdapter(adapterPiutang);
                        break;




                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));



    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        super.dispatchTouchEvent(ev);
        //mScaleDetector.onTouchEvent(ev);
        //gestureDetector.onTouchEvent(ev);
        detector.onTouchEvent(ev);
        switch (ev.getAction()) {

            case MotionEvent.ACTION_DOWN:
                mx = ev.getX();
                my = ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                curX = ev.getX();
                curY = ev.getY();
                vScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                hScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                mx = curX;
                my = curY;
                break;
            case MotionEvent.ACTION_UP:
                curX = ev.getX();
                curY = ev.getY();
                vScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                hScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                break;
        }

        return true;
    }

    private void fetchJenis(final String uid,final String cif,int post) {
        String endpoint = "";
        switch (post){
            case 0:
                endpoint = AppConfig.M_SIM_JENIS+"?uid="+uid+"&kode=JenisSimpanan&cif="+cif;
                break;
            case 1:
                endpoint = AppConfig.M_DEP_JENIS+"?uid="+uid+"&kode=ItemJenis&cif="+cif;
                break;
            case 2:
                endpoint = AppConfig.M_HUT_JENIS+"?uid="+uid+"&kode=ItemJenis&cif="+cif;
                break;
            case 3:
                endpoint = AppConfig.M_PIU_JENIS+"?uid="+uid+"&kode=ItemJenis&cif="+cif;
                break;
        }

        JsonArrayRequest req = new JsonArrayRequest(endpoint,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        spinnerData.clear();
                        spinnerData.add(new SpinnerData("10020202","-- Pilih Jenis --"));
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                SpinnerData a = new SpinnerData(object.getString("ACC"),object.getString("ACC")+" "+object.getString("KETERANGAN"));
                                spinnerData.add(a);

                            } catch (JSONException e) {
                                Log.e("ero", "Json parsing error: " + e.getMessage());
                            }
                        }
                        adapterSpiner.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Erorr", "Error: " + error.getMessage());
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);

    }


    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {


        float onScaleBegin = 0;
        float onScaleEnd = 0;

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scale *= detector.getScaleFactor();
            rv2.setScaleX(scale);
            rv2.setScaleY(scale);
            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {

            //Toast.makeText(getApplicationContext(),"Scale Begin" ,Toast.LENGTH_SHORT).show();
            onScaleBegin = scale;

            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {

            //Toast.makeText(getApplicationContext(),"Scale Ended",Toast.LENGTH_SHORT).show();
            onScaleEnd = scale;

            if (onScaleEnd > onScaleBegin){
                //  Toast.makeText(getApplicationContext(),"Scaled Up by a factor of  " + String.valueOf( onScaleEnd / onScaleBegin ), Toast.LENGTH_SHORT  ).show();
            }

            if (onScaleEnd < onScaleBegin){
                //Toast.makeText(getApplicationContext(),"Scaled Down by a factor of  " + String.valueOf( onScaleBegin / onScaleEnd ), Toast.LENGTH_SHORT  ).show();
            }

            super.onScaleEnd(detector);
        }
    }


    private void fetchLaporanSimpanan(final String uid,final String cif,final String jenis) {
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.M_SIM_JENIS+"?cif="+cif+"&uid="+uid+"&kode=MasterSimpanan&jenis="+jenis, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    listSimpanan.clear();
                    ArrayList<String> sheader = new ArrayList<>();
                    sheader.add("NOSIMP");
                    sheader.add("CIB");
                    sheader.add("NAMA");
                    sheader.add("DEBET");
                    sheader.add("KREDIT");
                    sheader.add("SALDO");
                    listSimpanan.add(new Pinjaman(sheader,Pinjaman.TYPE_SUBHEADER));

                    JSONArray jObj = new JSONArray(response);
                    for(int i = 0;i<jObj.length();i++) {
                        JSONObject object = jObj.getJSONObject(i);
                        listSimpanan.add(new Pinjaman(i, object.getString("NOSIMP"), object.getString("CIB"), object.getString("NAMA"), object.getString("DEBET"), object.getString("KREDIT"), object.getString("SALDO")));
                    }
                    adapterSimpanan.notifyDataSetChanged();



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
                showSnackbar();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void showSnackbar(){
        final Snackbar snackbar = Snackbar.make(cl,"Terjadi Kesalahan Jaringan",Snackbar.LENGTH_INDEFINITE)
                .setAction("COBA LAGI", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (pil){
                            case 0:
                                fetchLaporanSimpanan(sesion.getNAMA(),sesion.getUID(),temp);

                                break;
                            case 1:
                                fetchLaporanDeposito(sesion.getNAMA(),sesion.getUID(),temp);

                                break;
                            case 2:
                                fetchLaporanHutang(sesion.getNAMA(),sesion.getUID(),temp);

                                break;
                            case 3:
                                fetchLaporanPiutang(sesion.getNAMA(),sesion.getUID(),temp);
                                break;
                        }
                    }
                });
        snackbar.show();
    }

    private void fetchLaporanHutang(final String uid,final String cif,final String jenis) {
        String end = "?cif="+cif+"&uid="+uid+"&kode=Preview&ki=10&jenis="+jenis+"&kreditur=SEMUA";
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.M_HUT_JENIS+end, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    listSimpanan.clear();
                    ArrayList<String> sheader = new ArrayList<>();
                    sheader.add("NOHUT");
                    sheader.add("KKRD");
                    sheader.add("NAMA KREDITUR");
                    sheader.add("JATUH TEMPO");
                    sheader.add("PLAFOND");
                    sheader.add("OUTSTANDING");
                    listSimpanan.add(new Pinjaman(sheader,Pinjaman.TYPE_SUBHEADER));

                    JSONObject a = new JSONObject(response);
                    JSONArray jObj = a.getJSONArray("table");
                    for(int i = 0;i<jObj.length();i++) {
                        JSONObject object = jObj.getJSONObject(i);
                        listSimpanan.add(new Pinjaman(i, object.getString("NOHUT"), object.getString("KKRD"), object.getString("NAMA_KREDITUR"), object.getString("JATUHTEMPO"), object.getString("PLAFOND"), object.getString("OUTSTANDING")));
                    }
                    adapterSimpanan.notifyDataSetChanged();



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
                showSnackbar();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


    }

    private void fetchLaporanPiutang(final String uid,final String cif,final String jenis) {

        String end = "?cif="+cif+"&uid="+uid+"&kode=Preview&ki=10&jenis="+jenis+"&ki=SEMUA";
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.M_PIU_JENIS+end, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    listPiutang.clear();
                    ArrayList<String> sheader = new ArrayList<>();
                    sheader.add("NOPINJ");
                    sheader.add("NAMA");
                    sheader.add("JATUH TEMPO");
                    sheader.add("KE");
                    sheader.add("SISA");
                    sheader.add("PLAFOND");
                    sheader.add("OUTSTANDING");
                    listPiutang.add(new Piutang(sheader,Piutang.TYPE_SUBHEADER));
                    Log.d("panjang array","panjang array"+listPiutang.get(0).getHeader().size());

                    JSONObject a = new JSONObject(response);
                    JSONArray jObj = a.getJSONArray("table");
                    for(int i = 0;i<jObj.length();i++) {
                        JSONObject object = jObj.getJSONObject(i);
                        listPiutang.add(new Piutang(i, object.getString("NOPINJ"), object.getString("NAMA"), object.getString("JATUHTEMPO"), object.getString("KE"), object.getString("SISA"), object.getString("PLAFOND"), object.getString("OUTSTANDING")));
                    }
                    ArrayList<String> sheader2 = new ArrayList<>();
                    sheader2.add("TOTAL");
                    JSONObject total = a.getJSONObject("total");
                    sheader2.add(total.getString("TOTAL"));

                    listPiutang.add(new Piutang(sheader2,Piutang.TYPE_TOTAL));
                    adapterPiutang.notifyDataSetChanged();


                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
                showSnackbar();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void fetchLaporanDeposito(final String uid,final String cif,final String jenis) {
        String end = "?cif="+cif+"&uid="+uid+"&kode=Preview&ki=10&jenis="+jenis+"&ki=SEMUA";
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.M_DEP_JENIS+end, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    listDeposito.clear();
                    ArrayList<String> sheader = new ArrayList<>();
                    sheader.add("NODEP");
                    sheader.add("CIB");
                    sheader.add("NAMA");
                    sheader.add("ACC");
                    sheader.add("KETERANGAN");
                    sheader.add("NOMINAL");
                    sheader.add("BUNGA");
                    sheader.add("PAJAK");
                    listDeposito.add(new Deposito(sheader,Pinjaman.TYPE_SUBHEADER));
                    JSONObject a = new JSONObject(response);
                    JSONArray jObj = a.getJSONArray("table");
                    for(int i = 0;i<jObj.length();i++) {
                        JSONObject object = jObj.getJSONObject(i);
                        listDeposito.add(new Deposito(i, object.getString("NODEP"), object.getString("CIB"), object.getString("NAMA"), object.getString("ACC"), object.getString("KETERANGAN"), object.getString("NOMINAL"), object.getString("BUNGA"), object.getString("PAJAK")));
                    }
                    adapterDeposito.notifyDataSetChanged();



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
                showSnackbar();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showNoInternet(isConnected);
    }

    private void showNoInternet(boolean status) {
        final Snackbar snackbar = Snackbar.make(cl,"Tidak Ada Konektifitas",Snackbar.LENGTH_INDEFINITE)
                .setAction("Aktifkan", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setAction(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS);
                        startActivity(intent);
                    }
                });

        if(!status){
            snackbar.show();
        }else{
            snackbar.dismiss();
        }


    }

}
