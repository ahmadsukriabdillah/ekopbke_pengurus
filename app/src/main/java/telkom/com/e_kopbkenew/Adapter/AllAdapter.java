package telkom.com.e_kopbkenew.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import telkom.com.e_kopbkenew.Data.AllData;
import telkom.com.e_kopbkenew.Data.Pinjaman;
import telkom.com.e_kopbkenew.R;

import static telkom.com.e_kopbkenew.Data.Neraca.TYPE_CONTENT;
import static telkom.com.e_kopbkenew.Data.Neraca.TYPE_HEADER;
import static telkom.com.e_kopbkenew.Data.Neraca.TYPE_SUBHEADER;
import static telkom.com.e_kopbkenew.Data.Neraca.TYPE_TOTAL;

/**
 * Created by sukri on 27/10/16.
 */

public class AllAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {

private Context mContext;
private List<AllData> albumList;

private static class ContentHolder extends RecyclerView.ViewHolder {
    public TextView c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15;
    public ContentHolder(View view) {
        super(view);
        c1 = (TextView) view.findViewById(R.id.content1);
        c2 = (TextView) view.findViewById(R.id.content2);
        c3 = (TextView) view.findViewById(R.id.content3);
        c4 = (TextView) view.findViewById(R.id.content4);
        c5 = (TextView) view.findViewById(R.id.content5);
        c6 = (TextView) view.findViewById(R.id.content6);
        c7 = (TextView) view.findViewById(R.id.content7);
        c8 = (TextView) view.findViewById(R.id.content8);
        c9 = (TextView) view.findViewById(R.id.content9);
        c10 = (TextView) view.findViewById(R.id.content10);
        c11 = (TextView) view.findViewById(R.id.content11);
        c12 = (TextView) view.findViewById(R.id.content12);
        c13 = (TextView) view.findViewById(R.id.content13);
        c14 = (TextView) view.findViewById(R.id.content14);
        c15 = (TextView) view.findViewById(R.id.content15);


    }


}
    private static class SubheaderContent extends RecyclerView.ViewHolder {
        public  TextView subheader1,subheader2,subheader3,subheader4,subheader5,subheader6,subheader7,subheader8,subheader9,subheader10,subheader11,subheader12,subheader13,subheader14,subheader15;
        public SubheaderContent(View view) {
        super(view);
            subheader1 = (TextView) view.findViewById(R.id.sub1);
            subheader2 = (TextView) view.findViewById(R.id.sub2);
            subheader3 = (TextView) view.findViewById(R.id.sub3);
            subheader4 = (TextView) view.findViewById(R.id.sub4);
            subheader5 = (TextView) view.findViewById(R.id.sub5);
            subheader6 = (TextView) view.findViewById(R.id.sub6);
            subheader7 = (TextView) view.findViewById(R.id.sub7);
            subheader8 = (TextView) view.findViewById(R.id.sub8);
            subheader9 = (TextView) view.findViewById(R.id.sub9);
            subheader10 = (TextView) view.findViewById(R.id.sub10);
            subheader11 = (TextView) view.findViewById(R.id.sub11);
            subheader12 = (TextView) view.findViewById(R.id.sub12);
            subheader13 = (TextView) view.findViewById(R.id.sub13);
            subheader14 = (TextView) view.findViewById(R.id.sub14);
            subheader15 = (TextView) view.findViewById(R.id.sub15);


    }


}

    public AllAdapter(Context mContext, ArrayList<AllData> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        switch (viewType){
            case TYPE_SUBHEADER:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_produktifitas_header, parent, false);
                return new SubheaderContent(itemView);

            case TYPE_CONTENT:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_produktifitas_content, parent, false);
                return new ContentHolder(itemView);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        AllData album = albumList.get(position);


        if (album != null) {
            switch (album.getViewType()) {

                case TYPE_SUBHEADER:
                    ((SubheaderContent) holder).subheader1.setText(album.getHeader().get(0));
                    ((SubheaderContent) holder).subheader2.setText(album.getHeader().get(1));
                    ((SubheaderContent) holder).subheader3.setText(album.getHeader().get(2));
                    ((SubheaderContent) holder).subheader4.setText(album.getHeader().get(3));
                    ((SubheaderContent) holder).subheader5.setText(album.getHeader().get(4));
                    ((SubheaderContent) holder).subheader6.setText(album.getHeader().get(5));
                    ((SubheaderContent) holder).subheader7.setText(album.getHeader().get(6));
                    ((SubheaderContent) holder).subheader8.setText(album.getHeader().get(7));
                    ((SubheaderContent) holder).subheader9.setText(album.getHeader().get(8));
                    ((SubheaderContent) holder).subheader10.setText(album.getHeader().get(9));
                    ((SubheaderContent) holder).subheader11.setText(album.getHeader().get(10));
                    ((SubheaderContent) holder).subheader12.setText(album.getHeader().get(11));
                    ((SubheaderContent) holder).subheader13.setText(album.getHeader().get(12));
                    ((SubheaderContent) holder).subheader14.setText(album.getHeader().get(13));
                    ((SubheaderContent) holder).subheader15.setText(album.getHeader().get(14));
                    break;
                case TYPE_CONTENT:
                    ((ContentHolder) holder).c1.setText(album.getHeader().get(0));
                    ((ContentHolder) holder).c2.setText(album.getHeader().get(1));
                    ((ContentHolder) holder).c3.setText(album.getHeader().get(2));
                    ((ContentHolder) holder).c4.setText(album.getHeader().get(3));
                    ((ContentHolder) holder).c5.setText(album.getHeader().get(4));
                    ((ContentHolder) holder).c6.setText(album.getHeader().get(5));
                    ((ContentHolder) holder).c7.setText(album.getHeader().get(6));
                    ((ContentHolder) holder).c8.setText(album.getHeader().get(7));
                    ((ContentHolder) holder).c9.setText(album.getHeader().get(8));
                    ((ContentHolder) holder).c10.setText(album.getHeader().get(9));
                    ((ContentHolder) holder).c11.setText(album.getHeader().get(10));
                    ((ContentHolder) holder).c12.setText(album.getHeader().get(11));
                    ((ContentHolder) holder).c13.setText(album.getHeader().get(12));
                    ((ContentHolder) holder).c14.setText(album.getHeader().get(13));
                    ((ContentHolder) holder).c15.setText(album.getHeader().get(14));
                    break;
            }
        }

    }

    @Override
    public int getItemCount() {
        if(albumList == null){
            return 0;
        }
        return albumList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (albumList != null) {
            AllData object = albumList.get(position);
            if (object != null) {
                return object.getViewType();
            }
        }
        return 0;
    }
}
