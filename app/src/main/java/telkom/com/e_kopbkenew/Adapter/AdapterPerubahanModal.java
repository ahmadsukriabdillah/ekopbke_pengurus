package telkom.com.e_kopbkenew.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import telkom.com.e_kopbkenew.Data.CFlow;
import telkom.com.e_kopbkenew.Data.PModal;
import telkom.com.e_kopbkenew.R;

import static telkom.com.e_kopbkenew.Data.CFlow.TYPE_CONTENT;
import static telkom.com.e_kopbkenew.Data.CFlow.TYPE_HEADER;

/**
 * Created by sukri on 27/10/16.
 */

public class AdapterPerubahanModal extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {

private Context mContext;
private List<PModal> albumList;

private static class ContentHolder extends RecyclerView.ViewHolder {
    public TextView keterangan,perubahan,saldo;
    public ContentHolder(View view) {
        super(view);
        perubahan = (TextView) view.findViewById(R.id.content2);
        keterangan = (TextView) view.findViewById(R.id.content1);

        saldo = (TextView) view.findViewById(R.id.content3);


    }


}
    private static class SubheaderContent extends RecyclerView.ViewHolder {
        public  TextView subheader1,subheader2,subheader3;
        public SubheaderContent(View view) {
        super(view);
            subheader1 = (TextView) view.findViewById(R.id.sub1);
            subheader2 = (TextView) view.findViewById(R.id.sub2);
            subheader3 = (TextView) view.findViewById(R.id.sub3);



    }


}


    public AdapterPerubahanModal(Context mContext, ArrayList<PModal> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        switch (viewType){
            case TYPE_HEADER:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_pm_header, parent, false);
                return new SubheaderContent(itemView);

            case TYPE_CONTENT:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_pm_content, parent, false);
                return new ContentHolder(itemView);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        PModal album = albumList.get(position);


        if (album != null) {
            switch (album.getViewType()) {

                case TYPE_HEADER:
                    ((SubheaderContent) holder).subheader1.setText(album.getHeader().get(0));
                    ((SubheaderContent) holder).subheader2.setText(album.getHeader().get(1));
                    ((SubheaderContent) holder).subheader3.setText(album.getHeader().get(2));
                    break;
                case TYPE_CONTENT:
                    ((ContentHolder) holder).perubahan.setText(album.getPerubahan());
                    ((ContentHolder) holder).keterangan.setText(album.getKeterangan());
                    ((ContentHolder) holder).saldo.setText(album.getSaldo());
                    break;
            }
        }

    }

    @Override
    public int getItemCount() {
        if(albumList == null){
            return 0;
        }
        return albumList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (albumList != null) {
            PModal object = albumList.get(position);
            if (object != null) {
                return object.getViewType();
            }
        }
        return 0;
    }
}
