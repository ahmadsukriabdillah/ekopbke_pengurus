package telkom.com.e_kopbkenew.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import telkom.com.e_kopbkenew.Data.Anggota;
import telkom.com.e_kopbkenew.Data.Profile;
import telkom.com.e_kopbkenew.R;


/**
 * Created by sukri on 24/10/16.
 */

public class AdapterAnggota extends RecyclerView.Adapter<AdapterAnggota.MyViewHolder> implements Filterable {

    private Context mContext;
    private List<Anggota> albumList;
    private List<Anggota> albumListFilter;

    @Override
    public Filter getFilter() {
        AnggotaFilter filter = new AnggotaFilter();
        return filter;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView judul, keterangan;
        public MyViewHolder(View view) {
            super(view);
            judul = (TextView) view.findViewById(R.id.judul);
            keterangan = (TextView) view.findViewById(R.id.keterangan);


        }
    }


    public AdapterAnggota(Context mContext, ArrayList<Anggota> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
        this.albumListFilter = albumList;
    }
    public Anggota gItm(int po){
        return albumListFilter.get(po);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_anggota, parent, false);
        return new MyViewHolder(itemView);
    }

    private class AnggotaFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Anggota> filteredData = new ArrayList<>();
            FilterResults result = new FilterResults();
            String filterString = constraint.toString().toLowerCase();
            for(Anggota mhs: albumList){
                if(mhs.getNAMA().toLowerCase().contains(filterString) || mhs.getCIB().toLowerCase().contains(filterString) || mhs.getNAMA_INSTANSI().toLowerCase().contains(filterString)){
                    filteredData.add(mhs);
                }
            }
            result.count = filteredData.size();
            result.values =  filteredData;
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            albumListFilter = (List<Anggota>) results.values;
            notifyDataSetChanged();
        }

    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Anggota album = albumListFilter.get(position);
        holder.judul.setText(album.getCIB()+" "+album.getNAMA());
        holder.keterangan.setText(album.getNAMA_INSTANSI());
    }

    @Override
    public int getItemCount() {
        return albumListFilter.size();
    }
    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }
    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private AdapterAnggota.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final AdapterAnggota.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
