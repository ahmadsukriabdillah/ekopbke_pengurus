package telkom.com.e_kopbkenew.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import telkom.com.e_kopbkenew.Data.Neraca;
import telkom.com.e_kopbkenew.Data.NeracaLajur;
import telkom.com.e_kopbkenew.R;

import static telkom.com.e_kopbkenew.Data.Neraca.TYPE_CONTENT;
import static telkom.com.e_kopbkenew.Data.Neraca.TYPE_HEADER;
import static telkom.com.e_kopbkenew.Data.Neraca.TYPE_SUBHEADER;
import static telkom.com.e_kopbkenew.Data.Neraca.TYPE_TOTAL;

/**
 * Created by sukri on 27/10/16.
 */

public class AdapterNeracaLajur extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {

private Context mContext;
private List<NeracaLajur> albumList;

    private static class ContentHolder extends RecyclerView.ViewHolder {
    public TextView acc,keterangan,saad,saak,md,mk,sad,sak,lb,lp,na,nke;
    public ContentHolder(View view) {
        super(view);
        acc = (TextView) view.findViewById(R.id.content1);
        keterangan = (TextView) view.findViewById(R.id.content2);
        saad = (TextView) view.findViewById(R.id.content3);
        saak = (TextView) view.findViewById(R.id.content4);
        md = (TextView) view.findViewById(R.id.content5);
        mk = (TextView) view.findViewById(R.id.content6);
        sad = (TextView) view.findViewById(R.id.content7);
        sak = (TextView) view.findViewById(R.id.content8);
        lb = (TextView) view.findViewById(R.id.content9);
        lp = (TextView) view.findViewById(R.id.content10);
        na = (TextView) view.findViewById(R.id.content11);
        nke = (TextView) view.findViewById(R.id.content12);
    }





    }

    public static class HeaderHolder extends RecyclerView.ViewHolder {
        public HeaderHolder(View itemView) {
            super(itemView);
        }
    }
    private static class TotalContent extends RecyclerView.ViewHolder {
        public TextView total1, total2, total3, total4,total5,total6,total7,total8,total9,total10;

        public TotalContent(View view) {
            super(view);
            total1 = (TextView) view.findViewById(R.id.total2);
            total2 = (TextView) view.findViewById(R.id.total3);
            total3 = (TextView) view.findViewById(R.id.total4);
            total4 = (TextView) view.findViewById(R.id.total5);
            total5 = (TextView) view.findViewById(R.id.total6);
            total6 = (TextView) view.findViewById(R.id.total7);
            total7 = (TextView) view.findViewById(R.id.total8);
            total8 = (TextView) view.findViewById(R.id.total9);
            total9 = (TextView) view.findViewById(R.id.total10);
            total10 = (TextView) view.findViewById(R.id.total11);
        }
    }
   public AdapterNeracaLajur(Context mContext, ArrayList<NeracaLajur> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        switch (viewType){
            case TYPE_HEADER:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_header_lajur, parent, false);
                return new AdapterNeracaLajur.HeaderHolder(itemView);

            case TYPE_CONTENT:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_content_lajur, parent, false);
                return new AdapterNeracaLajur.ContentHolder(itemView);
            case TYPE_TOTAL:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_total_lajur, parent, false);
                return new AdapterNeracaLajur.TotalContent(itemView);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NeracaLajur album = albumList.get(position);


        if (album != null) {
            switch (album.getViewType()) {
                case TYPE_HEADER:
                    break;
                case TYPE_CONTENT:
                    ((ContentHolder) holder).acc.setText(album.getAcc());
                    ((ContentHolder) holder).keterangan.setText(album.getDeskripsi());
                    ((ContentHolder) holder).saad.setText(album.getSaad());
                    ((ContentHolder) holder).saak.setText(album.getSaak());
                    ((ContentHolder) holder).md.setText(album.getMd());
                    ((ContentHolder) holder).mk.setText(album.getMk());
                    ((ContentHolder) holder).sad.setText(album.getSad());
                    ((ContentHolder) holder).sak.setText(album.getSak());
                    ((ContentHolder) holder).lb.setText(album.getLb());
                    ((ContentHolder) holder).lp.setText(album.getLp());
                    ((ContentHolder) holder).na.setText(album.getNa());
                    ((ContentHolder) holder).nke.setText(album.getNke());
                    break;
                case TYPE_TOTAL:
                    ((TotalContent) holder).total1.setText(album.getHeader().get(0));
                    ((TotalContent) holder).total2.setText(album.getHeader().get(1));
                    ((TotalContent) holder).total3.setText(album.getHeader().get(2));
                    ((TotalContent) holder).total4.setText(album.getHeader().get(3));
                    ((TotalContent) holder).total5.setText(album.getHeader().get(4));
                    ((TotalContent) holder).total6.setText(album.getHeader().get(5));
                    ((TotalContent) holder).total7.setText(album.getHeader().get(6));
                    ((TotalContent) holder).total8.setText(album.getHeader().get(7));
                    ((TotalContent) holder).total9.setText(album.getHeader().get(8));
                    ((TotalContent) holder).total10.setText(album.getHeader().get(9));
                    break;
            }
        }

    }

    @Override
    public int getItemCount() {
        if(albumList == null){
            return 0;
        }
        return albumList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (albumList != null) {
            NeracaLajur object = albumList.get(position);
            if (object != null) {
                return object.getViewType();
            }
        }
        return 0;
    }
}
