package telkom.com.e_kopbkenew.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import telkom.com.e_kopbkenew.Data.Profile;
import telkom.com.e_kopbkenew.R;


/**
 * Created by sukri on 24/10/16.
 */

public class AdapterProfile extends RecyclerView.Adapter<AdapterProfile.MyViewHolder> {

    private Context mContext;
    private List<Profile> albumList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView judul, keterangan;
        public MyViewHolder(View view) {
            super(view);
            judul = (TextView) view.findViewById(R.id.judul);
            keterangan = (TextView) view.findViewById(R.id.keterangan);


        }
    }


    public AdapterProfile(Context mContext, ArrayList<telkom.com.e_kopbkenew.Data.Profile> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_profile_sum, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Profile album = albumList.get(position);
        holder.judul.setText(album.getJudul());
        holder.keterangan.setText(album.getKeterangan());

    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }
}
