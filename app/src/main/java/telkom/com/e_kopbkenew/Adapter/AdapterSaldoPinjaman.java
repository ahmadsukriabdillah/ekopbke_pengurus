package telkom.com.e_kopbkenew.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import telkom.com.e_kopbkenew.Data.Neraca;
import telkom.com.e_kopbkenew.Data.Pinjaman;
import telkom.com.e_kopbkenew.R;

import static telkom.com.e_kopbkenew.Data.Neraca.TYPE_CONTENT;
import static telkom.com.e_kopbkenew.Data.Neraca.TYPE_HEADER;
import static telkom.com.e_kopbkenew.Data.Neraca.TYPE_SUBHEADER;
import static telkom.com.e_kopbkenew.Data.Neraca.TYPE_TOTAL;

/**
 * Created by sukri on 27/10/16.
 */

public class AdapterSaldoPinjaman extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {

private Context mContext;
private List<Pinjaman> albumList;

private static class ContentHolder extends RecyclerView.ViewHolder {
    public TextView acc1,acc2,keterangan1,keterangan2,saldo1,saldo2;
    public ContentHolder(View view) {
        super(view);
        acc1 = (TextView) view.findViewById(R.id.content1);
        acc2 = (TextView) view.findViewById(R.id.content4);
        keterangan1 = (TextView) view.findViewById(R.id.content2);
        keterangan2 = (TextView) view.findViewById(R.id.content5);
        saldo1 = (TextView) view.findViewById(R.id.content3);
        saldo2 = (TextView) view.findViewById(R.id.content6);


    }


}
    private static class SubheaderContent extends RecyclerView.ViewHolder {
        public  TextView subheader1,subheader2,subheader3,subheader4,subheader5,subheader6;
        public SubheaderContent(View view) {
        super(view);
            subheader1 = (TextView) view.findViewById(R.id.sub1);
            subheader2 = (TextView) view.findViewById(R.id.sub2);
            subheader3 = (TextView) view.findViewById(R.id.sub3);
            subheader4 = (TextView) view.findViewById(R.id.sub4);
            subheader5 = (TextView) view.findViewById(R.id.sub5);
            subheader6 = (TextView) view.findViewById(R.id.sub6);


    }


}
    private static class TotalContent extends RecyclerView.ViewHolder {
        public TextView total1, total2, total3, total4;

        public TotalContent(View view) {
            super(view);
            total1 = (TextView) view.findViewById(R.id.total1);
            total2 = (TextView) view.findViewById(R.id.total2);
            total3 = (TextView) view.findViewById(R.id.total3);
            total4 = (TextView) view.findViewById(R.id.total4);


        }
    }

    public static class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView header1;
        private TextView header2;
        public HeaderHolder(View itemView) {
            super(itemView);
            header1 = (TextView) itemView.findViewById(R.id.header1);
            header2 = (TextView) itemView.findViewById(R.id.header2);
        }
    }
    public AdapterSaldoPinjaman(Context mContext, ArrayList<Pinjaman> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        switch (viewType){
            case TYPE_SUBHEADER:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_pinjaman_subheader, parent, false);
                return new SubheaderContent(itemView);

            case TYPE_CONTENT:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_pinjaman_content, parent, false);
                return new ContentHolder(itemView);
            case TYPE_TOTAL:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_pinjaman_content, parent, false);
                return new TotalContent(itemView);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Pinjaman album = albumList.get(position);


        if (album != null) {
            switch (album.getViewType()) {
                case TYPE_HEADER:
                    ((HeaderHolder) holder).header1.setText(album.getHeader().get(0));
                    ((HeaderHolder) holder).header2.setText(album.getHeader().get(1));
                    break;
                case TYPE_SUBHEADER:
                    ((SubheaderContent) holder).subheader1.setText(album.getHeader().get(0));
                    ((SubheaderContent) holder).subheader2.setText(album.getHeader().get(1));
                    ((SubheaderContent) holder).subheader3.setText(album.getHeader().get(2));
                    ((SubheaderContent) holder).subheader4.setText(album.getHeader().get(3));
                    ((SubheaderContent) holder).subheader5.setText(album.getHeader().get(4));
                    ((SubheaderContent) holder).subheader6.setText(album.getHeader().get(5));
                    break;
                case TYPE_CONTENT:
                    ((ContentHolder) holder).acc1.setText(album.getAcc1());
                    ((ContentHolder) holder).keterangan1.setText(album.getDeskripsi1());
                    ((ContentHolder) holder).saldo1.setText(album.getSaldo1());
                    ((ContentHolder) holder).acc2.setText(album.getAcc2());
                    ((ContentHolder) holder).keterangan2.setText(album.getDeskripsi2());
                    ((ContentHolder) holder).saldo2.setText(album.getSaldo2());
                    break;
                case TYPE_TOTAL:
                    ((TotalContent) holder).total1.setText(album.getHeader().get(0));
                    ((TotalContent) holder).total2.setText(album.getHeader().get(1));
                    ((TotalContent) holder).total3.setText(album.getHeader().get(2));
                    ((TotalContent) holder).total4.setText(album.getHeader().get(3));
                    break;
            }
        }

    }

    @Override
    public int getItemCount() {
        if(albumList == null){
            return 0;
        }
        return albumList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (albumList != null) {
            Pinjaman object = albumList.get(position);
            if (object != null) {
                return object.getViewType();
            }
        }
        return 0;
    }
}
