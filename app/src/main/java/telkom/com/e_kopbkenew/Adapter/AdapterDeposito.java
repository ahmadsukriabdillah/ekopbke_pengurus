package telkom.com.e_kopbkenew.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import telkom.com.e_kopbkenew.Data.Deposito;
import telkom.com.e_kopbkenew.Data.Pinjaman;
import telkom.com.e_kopbkenew.R;

import static telkom.com.e_kopbkenew.Data.Neraca.TYPE_CONTENT;
import static telkom.com.e_kopbkenew.Data.Neraca.TYPE_HEADER;
import static telkom.com.e_kopbkenew.Data.Neraca.TYPE_SUBHEADER;
import static telkom.com.e_kopbkenew.Data.Neraca.TYPE_TOTAL;

/**
 * Created by sukri on 27/10/16.
 */

public class AdapterDeposito extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {

private Context mContext;
private List<Deposito> albumList;

private static class ContentHolder extends RecyclerView.ViewHolder {
    public TextView c1,c2,c3,c4,c5,c6,c7,c8;
    public ContentHolder(View view) {
        super(view);
        c1 = (TextView) view.findViewById(R.id.content1);
        c2 = (TextView) view.findViewById(R.id.content2);
        c3 = (TextView) view.findViewById(R.id.content3);
        c4 = (TextView) view.findViewById(R.id.content4);
        c5 = (TextView) view.findViewById(R.id.content5);
        c6 = (TextView) view.findViewById(R.id.content6);
        c7 = (TextView) view.findViewById(R.id.content7);
        c8 = (TextView) view.findViewById(R.id.content8);


    }


}
    private static class SubheaderContent extends RecyclerView.ViewHolder {
        public  TextView subheader1,subheader2,subheader3,subheader4,subheader5,subheader6,subheader7,subheader8;
        public SubheaderContent(View view) {
        super(view);
            subheader1 = (TextView) view.findViewById(R.id.sub1);
            subheader2 = (TextView) view.findViewById(R.id.sub2);
            subheader3 = (TextView) view.findViewById(R.id.sub3);
            subheader4 = (TextView) view.findViewById(R.id.sub4);
            subheader5 = (TextView) view.findViewById(R.id.sub5);
            subheader6 = (TextView) view.findViewById(R.id.sub6);
            subheader7 = (TextView) view.findViewById(R.id.sub7);
            subheader8 = (TextView) view.findViewById(R.id.sub8);


    }


}
    private static class TotalContent extends RecyclerView.ViewHolder {
        public TextView total1, total2, total3, total4;

        public TotalContent(View view) {
            super(view);
            total1 = (TextView) view.findViewById(R.id.total1);
            total2 = (TextView) view.findViewById(R.id.total2);
            total3 = (TextView) view.findViewById(R.id.total3);
            total4 = (TextView) view.findViewById(R.id.total4);


        }
    }

    public static class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView header1;
        private TextView header2;
        public HeaderHolder(View itemView) {
            super(itemView);
            header1 = (TextView) itemView.findViewById(R.id.header1);
            header2 = (TextView) itemView.findViewById(R.id.header2);
        }
    }
    public AdapterDeposito(Context mContext, ArrayList<Deposito> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        switch (viewType){
            case TYPE_SUBHEADER:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_deposito_subheader, parent, false);
                return new SubheaderContent(itemView);

            case TYPE_CONTENT:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_deposito_content, parent, false);
                return new ContentHolder(itemView);
            case TYPE_TOTAL:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_deposito_content, parent, false);
                return new TotalContent(itemView);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Deposito album = albumList.get(position);


        if (album != null) {
            switch (album.getViewType()) {
                case TYPE_HEADER:
                    ((HeaderHolder) holder).header1.setText(album.getHeader().get(0));
                    ((HeaderHolder) holder).header2.setText(album.getHeader().get(1));
                    break;
                case TYPE_SUBHEADER:
                    ((SubheaderContent) holder).subheader1.setText(album.getHeader().get(0));
                    ((SubheaderContent) holder).subheader2.setText(album.getHeader().get(1));
                    ((SubheaderContent) holder).subheader3.setText(album.getHeader().get(2));
                    ((SubheaderContent) holder).subheader4.setText(album.getHeader().get(3));
                    ((SubheaderContent) holder).subheader5.setText(album.getHeader().get(4));
                    ((SubheaderContent) holder).subheader6.setText(album.getHeader().get(5));
                    ((SubheaderContent) holder).subheader7.setText(album.getHeader().get(6));
                    ((SubheaderContent) holder).subheader8.setText(album.getHeader().get(7));
                    break;
                case TYPE_CONTENT:
                    ((ContentHolder) holder).c1.setText(album.getContent1());
                    ((ContentHolder) holder).c2.setText(album.getContent2());
                    ((ContentHolder) holder).c3.setText(album.getContent3());
                    ((ContentHolder) holder).c4.setText(album.getContent4());
                    ((ContentHolder) holder).c5.setText(album.getContent5());
                    ((ContentHolder) holder).c6.setText(album.getContent6());
                    ((ContentHolder) holder).c7.setText(album.getContent7());
                    ((ContentHolder) holder).c8.setText(album.getContent8());
                    break;
                case TYPE_TOTAL:
                    ((TotalContent) holder).total1.setText(album.getHeader().get(0));
                    ((TotalContent) holder).total2.setText(album.getHeader().get(1));
                    ((TotalContent) holder).total3.setText(album.getHeader().get(2));
                    ((TotalContent) holder).total4.setText(album.getHeader().get(3));
                    break;
            }
        }

    }

    @Override
    public int getItemCount() {
        if(albumList == null){
            return 0;
        }
        return albumList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (albumList != null) {
            Deposito object = albumList.get(position);
            if (object != null) {
                return object.getViewType();
            }
        }
        return 0;
    }
}
