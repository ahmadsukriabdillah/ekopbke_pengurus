package telkom.com.e_kopbkenew.Fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.logging.Handler;

import telkom.com.e_kopbkenew.Adapter.HomeMenuAdapter;
import telkom.com.e_kopbkenew.CashFlow;
import telkom.com.e_kopbkenew.Data.HomeMenu;
import telkom.com.e_kopbkenew.Data.SlideData;
import telkom.com.e_kopbkenew.Helper.AppConfig;
import telkom.com.e_kopbkenew.Helper.AppController;
import telkom.com.e_kopbkenew.Helper.SessionManager;
import telkom.com.e_kopbkenew.LaporanNeraca;
import telkom.com.e_kopbkenew.LaporangGabungan;
import telkom.com.e_kopbkenew.MenuModalNew;
import telkom.com.e_kopbkenew.OpenSliderContent;
import telkom.com.e_kopbkenew.PilihAnggota;
import telkom.com.e_kopbkenew.R;

/**
 * Created by sukri on 26/10/16.
 */

public class Home extends BaseFragment implements BaseSliderView.OnSliderClickListener{
    private SessionManager session;
    private RecyclerView rv,rv2;
    private TextView nm_client;
    private HomeMenuAdapter adapter,adapter2;
    private ArrayList<HomeMenu> albumList,albumList2;
    private SliderLayout sliderShow;
    private ArrayList<SlideData> slideData;
    private ArrayList<String> slideimage;
    private NavigateView navigateView;
    public static Home newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        Home fragment = new Home();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof NavigateView) {
            Log.d("Annv - Fragment", "activity " + activity.getLocalClassName());
            navigateView = (NavigateView) activity;
        }else {
            throw new ClassCastException(activity.toString()
                    + " must implemenet MyListFragment.OnItemSelectedListener");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mButton != null) {
            mButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mFragmentNavigation != null) {
                        mFragmentNavigation.pushFragment(Home.newInstance(mInt+1));
                    }
                }
            });
            mButton.setText(getClass().getSimpleName() + " " + mInt);
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmenthome,container,false);
        rv = (RecyclerView) v.findViewById(R.id.rv);
        rv2 = (RecyclerView) v.findViewById(R.id.rv2);
        sliderShow = (SliderLayout) v.findViewById(R.id.slider);
        nm_client = (TextView) v.findViewById(R.id.nama_client);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        albumList = new ArrayList<>();
        albumList2 = new ArrayList<>();
        session = new SessionManager(getActivity());
        slideData = new ArrayList<>();
        slideimage = new ArrayList<>();

        adapter = new HomeMenuAdapter(getActivity(),albumList);
        adapter2 = new HomeMenuAdapter(getActivity(),albumList2);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 4);
        RecyclerView.LayoutManager mLayoutManager2 = new GridLayoutManager(getActivity(), 4);
        rv.setLayoutManager(mLayoutManager);
        rv2.setLayoutManager(mLayoutManager2);
        nm_client.setText(session.getNAMAClient());
        //rv.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setAdapter(adapter);
        rv2.setItemAnimator(new DefaultItemAnimator());
        rv2.setAdapter(adapter2);


        rv.addOnItemTouchListener(new HomeMenuAdapter.RecyclerTouchListener(getActivity(), rv, new HomeMenuAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent a;
                switch (position){

                    case 0:
                        startActivity(new Intent(getActivity(),PilihAnggota.class));
                        break;

                    case 1:
                        startActivity(new Intent(getActivity(),MenuModalNew.class));
                        break;

                    case 4:
                        a = new Intent(getActivity(),CashFlow.class);
                        startActivity(a);
                        break;
                    case 3:
                        a = new Intent(getActivity(),LaporanNeraca.class);
                        a.putExtra("laporan",2);
                        startActivity(a);
                        break;
                    case 6:
                        a = new Intent(getActivity(),LaporanNeraca.class);
                        a.putExtra("laporan",3);
                        startActivity(a);
                        break;
                    case 5:
                        a = new Intent(getActivity(),LaporanNeraca.class);
                        a.putExtra("laporan",5);
                        startActivity(a);
                        break;

                    case 7:
                        a = new Intent(getActivity(),LaporanNeraca.class);
                        a.putExtra("laporan",1);
                        startActivity(a);
                        break;
                    case 2:
                        a = new Intent(getActivity(),LaporanNeraca.class);
                        a.putExtra("laporan",4);
                        startActivity(a);
                        break;

                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        rv2.addOnItemTouchListener(new HomeMenuAdapter.RecyclerTouchListener(getActivity(), rv2, new HomeMenuAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent a;
                switch (position){

                    case 0:
                        a = new Intent(getActivity(),LaporangGabungan.class);
                        a.putExtra("laporan",position);
                        startActivity(a);
                        break;
                    case 1:
                        a = new Intent(getActivity(),LaporangGabungan.class);
                        a.putExtra("laporan",position);
                        startActivity(a);
                        break;
                    case 2:
                        a = new Intent(getActivity(),LaporangGabungan.class);
                        a.putExtra("laporan",position);
                        startActivity(a);
                        break;
                    case 3:
                        a = new Intent(getActivity(),LaporangGabungan.class);
                        a.putExtra("laporan",position);
                        startActivity(a);
                        break;
                    case 4:
                        a = new Intent(getActivity(),LaporangGabungan.class);
                        a.putExtra("laporan",position);
                        startActivity(a);
                        break;
                    case 5:
                        a = new Intent(getActivity(),LaporangGabungan.class);
                        a.putExtra("laporan",position);
                        startActivity(a);
                        break;
                    case 6:
                        a = new Intent(getActivity(),LaporangGabungan.class);
                        a.putExtra("laporan",position);
                        startActivity(a);
                        break;
                    case 7:
                        a = new Intent(getActivity(),LaporangGabungan.class);
                        a.putExtra("laporan",position);
                        startActivity(a);
                        break;
                    }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        sliderShow.setPresetTransformer(SliderLayout.Transformer.Stack);
        sliderShow.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderShow.setCustomAnimation(new DescriptionAnimation());
        sliderShow.setDuration(4000);
        final android.os.Handler a = new android.os.Handler();
        final Runnable z =  new Runnable() {
            @Override
            public void run() {
                a.postDelayed(this,3000);
                sliderShow.startAutoCycle();
            }
        };

        setUpMenu();
        getSlide();

    }

    private void setUpMenu() {
        albumList.add(new HomeMenu(R.drawable.master_anggota,"Master Anggota","1"));
        albumList.add(new HomeMenu(R.drawable.master_saldo,"Master Saldo","2"));
        albumList.add(new HomeMenu(R.drawable.komparasi_saldo,"Komparasi Saldo","3"));
        albumList.add(new HomeMenu(R.drawable.neraca_lajur,"Neraca Lajur","4"));

        albumList.add(new HomeMenu(R.drawable.cash_flow,"Cash Flow (Arus Kas)","5"));
        albumList.add(new HomeMenu(R.drawable.perubahan_modal,"Perubahan Modal","6"));
        albumList.add(new HomeMenu(R.drawable.laba_rugi,"Laporan Laba/Rugi","7"));
        albumList.add(new HomeMenu(R.drawable.neraca,"Laporan Neraca","8"));

        albumList2.add(new HomeMenu(R.drawable.kolektibilitas,"Kolektabilitas","1"));
        albumList2.add(new HomeMenu(R.drawable.neraca_lajur,"Neraca Lajur","1"));
        albumList2.add(new HomeMenu(R.drawable.cash_flow,"Cash Flow (Arus Kas)","2"));
        albumList2.add(new HomeMenu(R.drawable.perubahan_modal,"Perubahan Modal","3"));
        albumList2.add(new HomeMenu(R.drawable.laba_rugi,"Lapran Laba/Rugi","4"));
        albumList2.add(new HomeMenu(R.drawable.neraca,"Laporan Neraca","5"));
        albumList2.add(new HomeMenu(R.drawable.produktifitas,"Laporan Produktivitas","6"));

        albumList2.add(new HomeMenu(R.drawable.kesehatan,"Laporan Kesehatan","7"));
        adapter2.notifyDataSetChanged();
        adapter.notifyDataSetChanged();


    }

    private void getSlide() {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.SLIDE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());

                try {
                    slideimage.clear();
                    JSONArray jObj = new JSONArray(response);
                    for(int i = 0; i< jObj.length();i++){
                        JSONObject obj = jObj.getJSONObject(i);
                        slideimage.add(obj.getString("NAMA_FILE"));
                    }
                    start();

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("login", "Login Error: " + error.getMessage());
                Toast.makeText(getActivity(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


    }

    private void start(){
        for (int i = 0; i < slideimage.size(); i++) {
            DefaultSliderView textSliderView = new DefaultSliderView(getActivity());
            textSliderView.image(slideimage.get(i)).setScaleType(BaseSliderView.ScaleType.Fit).setOnSliderClickListener(this);
            sliderShow.addSlider(textSliderView);
        }

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        Intent a =  new Intent(getActivity(),OpenSliderContent.class);
        startActivity(a);


    }

    @Override
    public void onResume() {
        sliderShow.stopAutoCycle();
        super.onResume();
    }

    public interface NavigateView{
        public void switchTab(int index,boolean anim);
    }
}
