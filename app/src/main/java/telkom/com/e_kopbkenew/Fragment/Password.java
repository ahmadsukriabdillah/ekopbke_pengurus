package telkom.com.e_kopbkenew.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import telkom.com.e_kopbkenew.Helper.AppConfig;
import telkom.com.e_kopbkenew.Helper.AppController;
import telkom.com.e_kopbkenew.Helper.SessionManager;
import telkom.com.e_kopbkenew.R;

/**
 * Created by sukri on 26/10/16.
 */

public class Password extends BaseFragment {

    private EditText pslama,psbaru,psbaruconfirm;
    private SessionManager sesion;
    private Button ubah;

    public static Password  newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        Password fragment = new Password();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mButton != null) {
            mButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mFragmentNavigation != null) {
                        mFragmentNavigation.pushFragment(Password.newInstance(mInt+1));
                    }
                }
            });
            mButton.setText(getClass().getSimpleName() + " " + mInt);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmentpassword,container,false);
        pslama = (EditText) v.findViewById(R.id.pslama);
        psbaru = (EditText) v.findViewById(R.id.psbaru);
        psbaruconfirm = (EditText) v.findViewById(R.id.psbaru);
        ubah = (Button) v.findViewById(R.id.ubahps);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        sesion = new SessionManager(getActivity());
        ubah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pslama.getText().toString().trim().isEmpty()){
                    pslama.requestFocus();
                }else if(psbaru.getText().toString().trim().isEmpty()){
                    psbaru.requestFocus();
                }else if(psbaruconfirm.getText().toString().trim().isEmpty()){
                    psbaruconfirm.requestFocus();
                }else if(psbaru.getText().toString().trim() == psbaruconfirm.getText().toString().trim()){
                    psbaru.setText("");
                    psbaruconfirm.setText("");
                    Toast.makeText(getActivity(),"Password Tidak Sama",Toast.LENGTH_LONG).show();
                }else{
                    changePassword(sesion.getNAMA(),psbaru.getText().toString().trim(),pslama.getText().toString().trim());
                }
            }
        });


    }

    private void changePassword(final String uid,final String passbaru,final String passlama) {

        String tag_string_req = "req_login";

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.CHANGE_PASSWORD+"?uid="+uid+"&passwordlama="+passlama+"&passwordbaru="+passbaru+"&kode=Ubahpassword", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                Log.d("login", "Login Link: " + AppConfig.CHANGE_PASSWORD+"?uid="+uid+"&passwordlama="+passlama+"&passwordbaru="+passbaru+"&kode=Ubahpassword");

                try {
                    JSONObject jObj = new JSONObject(response);

                    int FLG = Integer.parseInt(jObj.getString("FLG"));

                    switch (FLG){
                        case 1:

                            Toast.makeText(getActivity(),jObj.getString("MSG"),Toast.LENGTH_LONG).show();
                            emptyField();

                            break;
                        case 0:
                            Toast.makeText(getActivity(),jObj.getString("MSG"),Toast.LENGTH_LONG).show();
                            emptyField();
                            break;
                        case 2:
                            Toast.makeText(getActivity(),jObj.getString("MSG"),Toast.LENGTH_LONG).show();
                            emptyField();
                            break;

                    }






                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("login", "Login Error: " + error.getMessage());
                Toast.makeText(getActivity(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void emptyField(){
        pslama.setText("");
        psbaruconfirm.setText("");
        psbaru.setText("");
        pslama.requestFocus();

    }


}
