package telkom.com.e_kopbkenew.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import telkom.com.e_kopbkenew.Adapter.AdapterProfile;
import telkom.com.e_kopbkenew.Helper.AppConfig;
import telkom.com.e_kopbkenew.Helper.AppController;
import telkom.com.e_kopbkenew.Helper.SessionManager;
import telkom.com.e_kopbkenew.R;
import telkom.com.e_kopbkenew.Data.Profile;

/**
 * Created by sukri on 26/10/16.
 */

public class FragmentProfile extends BaseFragment {
    private RecyclerView rv;
    private ProgressDialog pDialog;
    private AdapterProfile adapter;
    private ArrayList<telkom.com.e_kopbkenew.Data.Profile> albumList;
    private SessionManager session;

    public static FragmentProfile  newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        FragmentProfile fragment = new FragmentProfile();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mButton != null) {
            mButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mFragmentNavigation != null) {
                        mFragmentNavigation.pushFragment(FragmentProfile.newInstance(mInt+1));
                    }
                }
            });
            mButton.setText(getClass().getSimpleName() + " " + mInt);
        }

    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmentsumarry,container,false);
        rv = (RecyclerView) v.findViewById(R.id.rv);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        session = new SessionManager(getActivity());
        pDialog = new ProgressDialog(getActivity());
        albumList = new ArrayList<>();
        adapter = new AdapterProfile(getActivity(),albumList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(mLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setAdapter(adapter);
        getData(session.getUID());
    }

    private void getData(String uid) {
        String tag_string_req = "req_profile";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.PROFILE+uid, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();

                try {
                    JSONArray jArray = new JSONArray(response);
                    JSONObject obj = jArray.getJSONObject(0);
                    //albumList.add(new telkom.com.e_kopbkenew.Data.Profile("NAMA CLIENT",obj.getString("NAMA_CLIENT")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("NAMA CLIENT",obj.getString("NAMA_CLIENT")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("NO SPK",obj.getString("NO_SPK")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("AKTA",obj.getString("AKTA")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("IDCAB",obj.getString("IDCAB")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("ID_PROP",obj.getString("ID_PROP")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("ID_KAB",obj.getString("ID_KAB")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("ID_KEC",obj.getString("ID_KEC")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("ID_KEL",obj.getString("ID_KEL")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("KODE_POS",obj.getString("KODE_POS")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("ALAMAT",obj.getString("ALAMAT")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("LONGITUDE",obj.getString("LONGITUDE")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("LATTITUDE",obj.getString("LATTITUDE")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("TELEPON",obj.getString("TELEPON")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("FAX",obj.getString("FAX")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("EMAIL",obj.getString("EMAIL")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("WEBSITE",obj.getString("WEBSITE")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("NAMA_KETUA",obj.getString("NAMA_KETUA")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("TELEPON_KETUA",obj.getString("TELEPON_KETUA")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("NAMA_WAKIL_KETUA",obj.getString("NAMA_WAKIL_KETUA")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("TELEPON_WAKIL_KETUA",obj.getString("TELEPON_WAKIL_KETUA")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("NAMA_BENDAHARA",obj.getString("NAMA_BENDAHARA")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("TELEPON_BENDAHARA",obj.getString("TELEPON_BENDAHARA")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("NAMA_PIC",obj.getString("NAMA_PIC")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("TELEPON_PIC",obj.getString("TELEPON_PIC")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("EMAIL_PIC",obj.getString("EMAIL_PIC")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("ANGGOTA",obj.getString("ANGGOTA")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("IP_ADDRESS",obj.getString("IP_ADDRESS")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("TELEPON_PIC",obj.getString("TELEPON_PIC")));
                    albumList.add(new telkom.com.e_kopbkenew.Data.Profile("ST",obj.getString("ST")));
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("login", "Login Error: " + error.getMessage());
                Toast.makeText(getActivity(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }




}
