package telkom.com.e_kopbkenew.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import telkom.com.e_kopbkenew.Helper.SessionManager;
import telkom.com.e_kopbkenew.R;

/**
 * Created by sukri on 26/10/16.
 */

public class Login extends BaseFragment {
    private Button btnLogin;
    private EditText inpEmail,inpPassword;
    private ProgressDialog pDialog;
    private SessionManager session;
    onLoginAttempt LoginAttempt;
    public static Login newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        Login fragment = new Login();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mButton != null) {
            mButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mFragmentNavigation != null) {
                        mFragmentNavigation.pushFragment(Password.newInstance(mInt+1));
                    }
                }
            });
            mButton.setText(getClass().getSimpleName() + " " + mInt);
        }

    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmentlogin,container,false);
        btnLogin = (Button) v.findViewById(R.id.button);
        inpEmail = (EditText) v.findViewById(R.id.eUsername);
        inpPassword = (EditText) v.findViewById(R.id.ePass);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = inpEmail.getText().toString().trim();
                String password = inpPassword.getText().toString().trim();
                if(username.isEmpty() || password.isEmpty()){
                    Toast.makeText(getActivity(),"Username / Password Masih Kosong", Toast.LENGTH_LONG).show();
                }else{
                    ((onLoginAttempt)getActivity()).check(username,password);
                }
            }
        });
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

}
