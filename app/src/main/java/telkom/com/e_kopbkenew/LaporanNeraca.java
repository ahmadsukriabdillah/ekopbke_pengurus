package telkom.com.e_kopbkenew;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import telkom.com.e_kopbkenew.Adapter.AdapterKomparasi;
import telkom.com.e_kopbkenew.Adapter.AdapterNeraca;
import telkom.com.e_kopbkenew.Adapter.AdapterNeracaLajur;
import telkom.com.e_kopbkenew.Adapter.AdapterPerubahanModal;
import telkom.com.e_kopbkenew.Data.Neraca;
import telkom.com.e_kopbkenew.Data.NeracaLajur;
import telkom.com.e_kopbkenew.Data.PModal;
import telkom.com.e_kopbkenew.Data.SpinnerData;
import telkom.com.e_kopbkenew.Helper.AppConfig;
import telkom.com.e_kopbkenew.Helper.AppController;
import telkom.com.e_kopbkenew.Helper.Connection;
import telkom.com.e_kopbkenew.Helper.SessionManager;

public class LaporanNeraca extends AppCompatActivity implements Connection.ConnectivityReceiverListener{

    private float mx, my;
    private float curX, curY;
    private CoordinatorLayout cl;
    private Spinner spinerJenis,spinerKodeUnit;
    private SessionManager session;
    private LinearLayout lyku;
    private LinearLayout lyjenis;
    private ArrayList<SpinnerData> dataJenis;
    private ArrayList<SpinnerData> dataKu;
    private ProgressDialog pDialog;
    private ArrayList<Neraca> neraca;
    private ArrayList<NeracaLajur> neracaLajur;
    private ArrayList<PModal> pModals;
    private AdapterNeraca adapter;
    private AdapterNeracaLajur adapterLajur;
    private AdapterKomparasi adapterKomparasi;
    private AdapterPerubahanModal adapterPerubahanModal;
    private RecyclerView rv;
    private ArrayAdapter<SpinnerData> jenis;
    private ArrayAdapter<SpinnerData> ku;
    private Toolbar toolbar;
    private ScrollView vScroll;
    private HorizontalScrollView hScroll;
    private static String temp;



    //gestur
    //private float mScale = 1f;
    //private ScaleGestureDetector mScaleDetector;
    //private float scale = 0.5f;
    private float scale = 1f;
    private ScaleGestureDetector detector;
    GestureDetector gestureDetector;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan_neraca);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        neraca = new ArrayList<>();
        neracaLajur = new ArrayList<>();
        pModals = new ArrayList<>();
        adapter = new AdapterNeraca(this,neraca);
        adapterLajur = new AdapterNeracaLajur(this,neracaLajur);
        adapterKomparasi = new AdapterKomparasi(this,neraca);
        adapterPerubahanModal = new AdapterPerubahanModal(this,pModals);
        session = new SessionManager(this);
        dataJenis = new ArrayList<>();
        dataKu = new ArrayList<>();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        spinerJenis = (Spinner) findViewById(R.id.spinner2);
        spinerKodeUnit = (Spinner) findViewById(R.id.spinner3);
        lyku = (LinearLayout) findViewById(R.id.kodeunit);
        lyjenis = (LinearLayout) findViewById(R.id.jenis);
        rv = (RecyclerView) findViewById(R.id.rv);
        cl = (CoordinatorLayout) findViewById(R.id.cl);
        vScroll = (ScrollView)findViewById(R.id.vScroll);
        hScroll = (HorizontalScrollView) findViewById(R.id.hScroll);
        //rv.setScaleX(0.3f);
        //rv.setScaleX(0.3f);
        //gestureDetector = new GestureDetector(this, new GestureListener());
        detector = new ScaleGestureDetector(this,new ScaleListener());


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setAutoMeasureEnabled(true);

        switch (getIntent().getIntExtra("laporan",0)){
            case 1:
                rv.setLayoutManager(mLayoutManager);
                toolbar.setTitle("Laporan Neraca");
                setSupportActionBar(toolbar);

                rv.setAdapter(adapter);
                break;
            case 2:
                rv.setLayoutManager(mLayoutManager);
                toolbar.setTitle("Laporan Neraca Lajur");
                setSupportActionBar(toolbar);

                rv.setAdapter(adapterLajur);
                break;


            case 5: //perubahan modal
                rv.setLayoutManager(mLayoutManager);
                toolbar.setTitle("Laporan Perubahan Modal");
                setSupportActionBar(toolbar);

                rv.setAdapter(adapterPerubahanModal);
                break;
            case 3:
                rv.setLayoutManager(mLayoutManager);
                toolbar.setTitle("Laporan Laba Rugi");
                setSupportActionBar(toolbar);

                rv.setAdapter(adapter);
                break;
            case 4:
                lyku.setVisibility(View.GONE);
                lyjenis.setVisibility(View.GONE);
                rv.setLayoutManager(mLayoutManager);
                toolbar.setTitle("Komparasi");
                setSupportActionBar(toolbar);
                rv.setAdapter(adapterKomparasi);
                laporanKomparasi(session.getNAMA(),session.getUID());

        }


        jenis = new ArrayAdapter<>(this,R.layout.spinner,dataJenis);
        jenis.setDropDownViewResource(R.layout.spinner); // The drop down view
        spinerJenis.setAdapter(jenis);

        spinerJenis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0){

                }else{

                    fetchKU(dataJenis.get(i).getId(),session.getUID());

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        ku = new ArrayAdapter<>(this,R.layout.spinner,dataKu);
        ku.setDropDownViewResource(R.layout.spinner); // The drop down view
        spinerKodeUnit.setAdapter(ku);

        spinerKodeUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0){

                }else {

                    switch (getIntent().getIntExtra("laporan",0)){
                        case 1:
                            laporanNeraca(session.getNAMA(),dataKu.get(i).getId(),session.getUID());
                            temp = dataKu.get(i).getId();
                            break;
                        case 2:
                            laporanNeracaLajur(session.getNAMA(),dataKu.get(i).getId(),session.getUID());
                            temp = dataKu.get(i).getId();
                            break;
                        case 3:
                            laporanLabaRugi(session.getNAMA(),dataKu.get(i).getId(),session.getUID());
                            temp = dataKu.get(i).getId();
                            break;
                        case 4:
                            laporanKomparasi(session.getNAMA(),session.getUID());
                            temp = dataKu.get(i).getId();

                            break;
                        case 5:
                            temp = dataKu.get(i).getId();
                            laporanPerubahanModal(session.getNAMA(),dataKu.get(i).getId(),session.getUID());
                            break;

                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        fetchJenis();
        /*
        mScaleDetector = new ScaleGestureDetector(this, new ScaleGestureDetector.SimpleOnScaleGestureListener()
        {
            @Override
            public boolean onScale(ScaleGestureDetector detector)
            {
                float scale = 1 - detector.getScaleFactor();

                float prevScale = mScale;
                mScale += scale;

                if (mScale < 0.1f) // Minimum scale condition:
                    mScale = 0.1f;

                if (mScale > 10f) // Maximum scale condition:
                    mScale = 10f;
                ScaleAnimation scaleAnimation = new ScaleAnimation(1f / prevScale, 1f / mScale, 1f / prevScale, 1f / mScale, detector.getFocusX(), detector.getFocusY());
                scaleAnimation.setDuration(0);
                scaleAnimation.setFillAfter(true);
                //layout =(HorizontalScrollView) findViewById(R.id.horisontal);
                rv.startAnimation(scaleAnimation);
                return true;
            }
        });*/

    }

    private void showSnackbar(){
        final Snackbar snackbar = Snackbar.make(cl,"Terjadi Kesalahan Jaringan",Snackbar.LENGTH_INDEFINITE)
                .setAction("COBA LAGI", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        switch (getIntent().getIntExtra("laporan",0)){
                            case 1:
                                laporanNeraca(session.getNAMA(),temp,session.getUID());
                                break;
                            case 2:
                                laporanNeracaLajur(session.getNAMA(),temp,session.getUID());
                                break;
                            case 3:
                                laporanLabaRugi(session.getNAMA(),temp,session.getUID());
                                break;
                            case 4:
                                laporanKomparasi(session.getNAMA(),session.getUID());
                                break;
                            case 5:
                                laporanPerubahanModal(session.getNAMA(),temp,session.getUID());
                                break;

                        }

                        //snackbar.dismiss();
                    }
                });
        snackbar.show();
    }


    private void laporanPerubahanModal(final String uid,final String ku,final String cif) {
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.LAPORAN_PERUBAHAN_MODAL+"?uid="+uid+"&ku="+ku+"&cif="+cif, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    pModals.clear();
                    ArrayList<String> sheader3 = new ArrayList<>();
                    sheader3.add("Keterangan");
                    sheader3.add("Perubahan");
                    sheader3.add("Saldo Akhir");

                    pModals.add(new PModal(sheader3,PModal.TYPE_HEADER));


                    JSONArray jObj = new JSONArray(response);
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);
                        pModals.add(new PModal(i,object.getString("URAIAN"),object.getString("PERUBAHAN"),object.getString("SALDO_AKHIR")));
                    }
                    adapterPerubahanModal.notifyDataSetChanged();



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
                showSnackbar();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void laporanLabaRugi(final String uid,final String ku,final String cif) {
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.LAPORAN_LABA_RUGI+"?uid="+uid+"&ku="+ku+"&cif="+cif+"&kode=Proses", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    neraca.clear();
                    ArrayList<String> sheader3 = new ArrayList<>();
                    sheader3.add("Beban");
                    sheader3.add("Pendapatan");

                    neraca.add(new Neraca(sheader3,Neraca.TYPE_HEADER));
                    ArrayList<String> sheader = new ArrayList<>();
                    sheader.add("ACC");
                    sheader.add("DESKRIPSI");
                    sheader.add("SALDO");
                    sheader.add("ACC");
                    sheader.add("DESKRIPSI");
                    sheader.add("SALDO");
                    neraca.add(new Neraca(sheader,Neraca.TYPE_SUBHEADER));

                    //neraca.add(new Neraca(2,new String[]{"Sub 1","Sub 2","Sub 3","Sub 4","Sub 5","Sub 6"}));
                    JSONObject jo = new JSONObject(response);
                    //JSONArray jObj = new JSONArray(response);
                    JSONArray jObj = jo.getJSONArray("table");
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);
                        neraca.add(new Neraca(i,object.getString("NO1"),object.getString("URAIAN1"),object.getString("ANOMINAL1"),object.getString("NO2"),object.getString("URAIAN2"),object.getString("ANOMINAL2")));
                    }
                    ArrayList<String> total = new ArrayList<>();
                    if(jo.getJSONObject("total").getString("TOTAL").equals("null")){

                    }else{
                        JSONObject jTot = jo.getJSONObject("total");
                        total.add("Total Debit");
                        total.add(jTot.getString("TOTALDEBET"));
                        total.add("Total Kredit");
                        total.add(jTot.getString("TOTALKREDIT"));
                        neraca.add(new Neraca(total,Neraca.TYPE_TOTAL));
                    }
                    //lyku.setVisibility(View.GONE);
                    //lyjenis.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),
                 //       error.getMessage(), Toast.LENGTH_LONG).show();
                showSnackbar();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


    }

    private void laporanKomparasi(final String uid,final String cif) {
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.KOMPARASi+"?uid="+uid+"&cif="+cif, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    neraca.clear();

                    ArrayList<String> sheader = new ArrayList<>();
                    sheader.add("MASTER");
                    sheader.add("ACC");
                    sheader.add("KETERANGAN");
                    sheader.add("SALDO MASTER");
                    sheader.add("SALDO NERACA");
                    sheader.add("SELISIH");
                    neraca.add(new Neraca(sheader,Neraca.TYPE_SUBHEADER));

                    //neraca.add(new Neraca(2,new String[]{"Sub 1","Sub 2","Sub 3","Sub 4","Sub 5","Sub 6"}));
                    //JSONObject jo = new JSONObject(response);
                    //JSONArray jObj = new JSONArray(response);
                    JSONArray jObj = new JSONArray(response);
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);
                        neraca.add(new Neraca(i,object.getString("MASTER"),object.getString("ACC"),object.getString("KETERANGAN"),object.getString("SALDO"),object.getString("NERACA"),object.getString("SELISIH")));
                    }
                    /*ArrayList<String> total = new ArrayList<>();
                    if(jo.isNull("total")){

                    }else{
                        JSONObject jTot = jo.getJSONObject("total");
                        total.add("Total Debit");
                        total.add(jTot.getString("TOTALDEBET"));
                        total.add("Total Kredit");
                        total.add(jTot.getString("TOTALKREDIT"));
                        neraca.add(new Neraca(total,Neraca.TYPE_TOTAL));
                    }
                    //lyku.setVisibility(View.GONE);
                    //lyjenis.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();
                    */
                    adapterKomparasi.notifyDataSetChanged();


                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
                showSnackbar();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


    }

    private void laporanNeracaLajur(final String uid,final String ku,final String cif) {
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.LAPORAN_NERACA_LAJUR+"?uid="+uid+"&ku="+ku+"&cif="+cif+"&kode=Preview", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    neracaLajur.clear();
                    ArrayList<String> sheader3 = new ArrayList<>();
                    sheader3.add("ASET");
                    sheader3.add("KEWAJIBAN DAN EKUITAS");

                    neracaLajur.add(new NeracaLajur(sheader3,Neraca.TYPE_HEADER));


                    //neraca.add(new Neraca(2,new String[]{"Sub 1","Sub 2","Sub 3","Sub 4","Sub 5","Sub 6"}));
                    JSONObject jo = new JSONObject(response);
                    //JSONArray jObj = new JSONArray(response);
                    JSONArray jObj = jo.getJSONArray("table");
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);
                        neracaLajur.add(new NeracaLajur(i,object.getString("ACC"),object.getString("KETERANGAN"),object.getString("DEBET"),object.getString("KREDIT"),object.getString("DEBET1"),object.getString("KREDIT1"),object.getString("DEBET2"),object.getString("KREDIT2"),object.getString("LDEBET"),object.getString("LKREDIT"),object.getString("NDEBET"),object.getString("NKREDIT")));
                    }

                    JSONObject jTot = jo.getJSONObject("total");
                    ArrayList<String> total = new ArrayList<>();

                    total.add(jTot.getString("TDEBET"));
                    total.add(jTot.getString("TKREDIT"));
                    total.add(jTot.getString("TDEBET1"));
                    total.add(jTot.getString("TKREDIT1"));
                    total.add(jTot.getString("TDEBET2"));
                    total.add(jTot.getString("TKREDIT2"));
                    total.add(jTot.getString("TLDEBET"));
                    total.add(jTot.getString("TLKREDIT"));
                    total.add(jTot.getString("NDEBET"));
                    total.add(jTot.getString("TNKREDIT"));
                    neracaLajur.add(new NeracaLajur(total,Neraca.TYPE_TOTAL));

                    //lyku.setVisibility(View.GONE);
                    //lyjenis.setVisibility(View.GONE);
                    adapterLajur.notifyDataSetChanged();



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("login", "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        super.dispatchTouchEvent(ev);
        //mScaleDetector.onTouchEvent(ev);
        //gestureDetector.onTouchEvent(ev);
        detector.onTouchEvent(ev);
        switch (ev.getAction()) {

            case MotionEvent.ACTION_DOWN:
                mx = ev.getX();
                my = ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                curX = ev.getX();
                curY = ev.getY();
                vScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                hScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                mx = curX;
                my = curY;
                break;
            case MotionEvent.ACTION_UP:
                curX = ev.getX();
                curY = ev.getY();
                vScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                hScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                break;
        }

        return true;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    private void laporanNeraca(final String uid,final String ku,final String cif) {
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.LAPORAN_NERACA+"?uid="+uid+"&ku="+ku+"&cif="+cif+"&kode=Proses", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    neraca.clear();
                    ArrayList<String> sheader3 = new ArrayList<>();
                    sheader3.add("ASET");
                    sheader3.add("KEWAJIBAN DAN EKUITAS");

                    neraca.add(new Neraca(sheader3,Neraca.TYPE_HEADER));
                    ArrayList<String> sheader = new ArrayList<>();
                    sheader.add("ACC");
                    sheader.add("DESKRIPSI");
                    sheader.add("SALDO");
                    sheader.add("ACC");
                    sheader.add("DESKRIPSI");
                    sheader.add("SALDO");
                    neraca.add(new Neraca(sheader,Neraca.TYPE_SUBHEADER));

                    //neraca.add(new Neraca(2,new String[]{"Sub 1","Sub 2","Sub 3","Sub 4","Sub 5","Sub 6"}));
                    JSONObject jo = new JSONObject(response);
                    //JSONArray jObj = new JSONArray(response);
                    JSONArray jObj = jo.getJSONArray("neraca");
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);
                        neraca.add(new Neraca(i,object.getString("NO1"),object.getString("URAIAN1"),object.getString("format(ANOMINAL1,2)"),object.getString("NO2"),object.getString("URAIAN2"),object.getString("format(ANOMINAL2,2)")));
                    }
                    JSONObject jTot = jo.getJSONObject("total");
                    ArrayList<String> total = new ArrayList<>();
                    total.add("Total Debit");
                    total.add(jTot.getString("TOTALDEBET"));
                    total.add("Total Kredit");
                    total.add(jTot.getString("TOTALKREDIT"));
                    neraca.add(new Neraca(total,Neraca.TYPE_TOTAL));
                    //lyku.setVisibility(View.GONE);
                    //lyjenis.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e("login", "Login Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),
                //        error.getMessage(), Toast.LENGTH_LONG).show();
                showSnackbar();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void fetchJenis() {
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.JENIS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    JSONArray jObj = new JSONArray(response);
                    dataJenis.add(new SpinnerData("0","-- Pilih Jenis Usaha --"));
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);
                        dataJenis.add(new SpinnerData(object.getString("ID"),object.getString("JENIS")));

                    }
                    jenis.notifyDataSetChanged();



                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("login", "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);



    }

    private void fetchKU(final String id,final String cif) {
        String tag_string_req = "req_login";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                AppConfig.KODEUNIT+cif+"&jenis="+id, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();


                try {
                    JSONArray jObj = new JSONArray(response);
                    dataKu.clear();
                    dataKu.add(new SpinnerData("0","-- Pilih Nama Unit --"));
                    for(int i = 0;i<jObj.length();i++){
                        JSONObject object = jObj.getJSONObject(i);
                        dataKu.add(new SpinnerData(object.getString("ku"),object.getString("nama_unit")));

                    }
                    ku.notifyDataSetChanged();
                    lyku.setVisibility(View.VISIBLE);


                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("login", "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    /*
    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }
        // event when double tap occurs
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            // double tap fired.
            return true;
        }
    }*/
    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {


        float onScaleBegin = 0;
        float onScaleEnd = 0;

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scale *= detector.getScaleFactor();
            rv.setScaleX(scale);
            rv.setScaleY(scale);
            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {

            //Toast.makeText(getApplicationContext(),"Scale Begin" ,Toast.LENGTH_SHORT).show();
            onScaleBegin = scale;

            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {

            //Toast.makeText(getApplicationContext(),"Scale Ended",Toast.LENGTH_SHORT).show();
            onScaleEnd = scale;

            if (onScaleEnd > onScaleBegin){
              //  Toast.makeText(getApplicationContext(),"Scaled Up by a factor of  " + String.valueOf( onScaleEnd / onScaleBegin ), Toast.LENGTH_SHORT  ).show();
            }

            if (onScaleEnd < onScaleBegin){
                //Toast.makeText(getApplicationContext(),"Scaled Down by a factor of  " + String.valueOf( onScaleBegin / onScaleEnd ), Toast.LENGTH_SHORT  ).show();
            }

            super.onScaleEnd(detector);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showNoInternet(isConnected);
    }

    private void showNoInternet(boolean status) {
        final Snackbar snackbar = Snackbar.make(cl,"Tidak Ada Konektifitas",Snackbar.LENGTH_INDEFINITE)
                .setAction("Aktifkan", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setAction(android.provider.Settings.ACTION_DATA_ROAMING_SETTINGS);
                        startActivity(intent);
                    }
                });

        if(!status){
            snackbar.show();
        }else{
            snackbar.dismiss();
        }


    }
}
