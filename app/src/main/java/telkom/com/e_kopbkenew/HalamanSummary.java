package telkom.com.e_kopbkenew;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import telkom.com.e_kopbkenew.Helper.AppConfig;
import telkom.com.e_kopbkenew.Helper.AppController;
import telkom.com.e_kopbkenew.Helper.SessionManager;

public class HalamanSummary extends AppCompatActivity {

    private SessionManager session;
    private ProgressDialog pDialog;
    private TextView txtMIF,txtTglLaporan,txtPeriodelaporan,txtAnggota,txtSimpanan,txtDeposito,txtPitang,txtHutang,txtAset,
            txtKewajiban,txtEkuitas,txtbulanLalu,txtBulanBerjalan;
    private LinearLayout LLv;
    private RecyclerView rv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman_summary);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        rv = (RecyclerView) findViewById(R.id.rv);
        LLv = (LinearLayout) findViewById(R.id.LLv);
        pDialog = new ProgressDialog(this);
        session = new SessionManager(this);
        txtMIF = (TextView) findViewById(R.id.txtMIF);
        txtTglLaporan = (TextView) findViewById(R.id.txtTglLap);
        txtPeriodelaporan = (TextView) findViewById(R.id.txtPeriodeLaporan);
        txtAnggota = (TextView) findViewById(R.id.txtAnggota);
        txtSimpanan = (TextView) findViewById(R.id.txtSimpanan);
        txtDeposito = (TextView) findViewById(R.id.txtDesposito);
        txtPitang = (TextView) findViewById(R.id.txtPiutang);
        txtHutang = (TextView) findViewById(R.id.txtHutang);
        txtAset = (TextView) findViewById(R.id.txtAset);
        txtKewajiban = (TextView) findViewById(R.id.txtKewajiban);
        txtEkuitas = (TextView) findViewById(R.id.txtEkuitas);
        txtbulanLalu = (TextView) findViewById(R.id.txtBulanLalu);
        txtBulanBerjalan = (TextView) findViewById(R.id.txtBulanBerjalan);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        checkLogin("Request",session.getUID());
    }

    private void checkLogin(final String kode, final String cif) {
        // Tag used to cancel the request
        String tag_string_req = "req_summary";

        pDialog.setMessage("Loading..");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.SUMMARY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("login", "Login Response: " + response.toString());
                hideDialog();

                try {
                    JSONArray jArray = new JSONArray(response);
                    JSONObject obj = jArray.getJSONObject(0);
                    txtMIF.setText(session.getUID());

                    txtTglLaporan.setText(obj.getString("tgllaporan"));
                    txtPeriodelaporan.setText(obj.getString("blnlaporan")+" / "+obj.getString("thnlaporan"));
                    txtAnggota.setText(obj.getString("jmlanggota"));
                    txtSimpanan.setText(obj.getString("totalsimpanan"));
                    txtDeposito.setText(obj.getString("totaldeposito"));
                    txtPitang.setText(obj.getString("totalpiutang"));
                    txtHutang.setText(obj.getString("totalhutang"));
                    txtAset.setText(obj.getString("totalaset"));
                    txtKewajiban.setText(obj.getString("totalkewajiban"));
                    txtEkuitas.setText(obj.getString("totalekuitas"));
                    txtbulanLalu.setText(obj.getString("totallrblnlalu"));
                    txtBulanBerjalan.setText(obj.getString("totallrberjalan"));
                    LLv.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("login", "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("kode", kode);
                params.put("cif", cif);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);


    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}